import re
import os
import json
from urllib import request
from sys import argv

BASE_DIR = os.path.dirname(os.path.realpath(__file__))
JSON_DIR = BASE_DIR + '/../bytepen/src/json'


class YouTubeGrabber:
    @staticmethod
    def build_url(category: str, fields: str = "", order: str = "", playlist_id: str = "", obj_type: str = "",
                  results: str = "50", part: str = "snippet"):
        base_url = "https://www.googleapis.com/youtube/v3/"
        channel = "channelId=UCvEimZmO3jz70KCd7W1CJZQ&"
        results = "maxResults={}&".format(results) if results != "" else ""
        api = "key={}&".format(os.environ["GOOGLE_API_KEY"])
        fields = "fields={}&".format(fields) if fields else ""
        order = "order={}&".format(order) if order else ""
        obj_type = "type={}&".format(obj_type) if obj_type else ""
        playlist_id = "playlistId={}&".format(playlist_id) if playlist_id else ""
        part = "part={}".format(part)

        return base_url + category + "?" + channel + results + api + fields + order + obj_type + playlist_id + part

    @staticmethod
    def write_json(file_name: str, content: dict):
        with open(JSON_DIR + "/{}.json".format(file_name), 'w') as fp:
            json.dump(content, fp)

    @staticmethod
    def query(url: str):
        with request.urlopen(url) as response:
            encoding = response.info().get_content_charset('utf-8')
            return json.loads(response.read().decode(encoding))

    def grab_all_playlists(self):
        fields = "items(id,snippet(title,thumbnails/medium,description))"
        playlists = self.query(self.build_url("playlists", fields))
        if include_tests:
            with open(BASE_DIR + '/../bytepen/test-json/Playlists.json') as tests:
                playlists['items'].extend(json.load(tests)['items'])
        for playlist in playlists['items']:
            if playlist['id'].startswith("TEST_"):
                with open(BASE_DIR + '/../bytepen/test-json/playlists/{}.json'.format(playlist['id'])) as tests:
                    self.write_json("playlists/{}".format(playlist['id']), json.load(tests))
            else:
                self.write_json("playlists/{}".format(playlist['id']), self.grab_videos_in_playlist(playlist['id']))
        return playlists

    def grab_latest_video(self):
        fields = "items(snippet(title,publishedAt,description,thumbnails/medium),id/videoId)"
        return self.query(self.build_url("search", fields, "date", "", "video", "1"))

    def grab_videos_in_playlist(self, playlist_id: str):
        fields = "items(snippet(title,position,resourceId/videoId,thumbnails/medium))"
        return self.query(self.build_url("playlistItems", fields, "", playlist_id, "playlistItem"))

    def grab_all_videos(self):
        fields = "nextPageToken,items(id(videoId),snippet(title,description))"
        url = self.build_url("search", fields, "date", "", "video")
        videos = self.query(url)
        items = videos['items']
        try:
            next_page = videos["nextPageToken"] if videos["nextPageToken"] else None
            while next_page:
                videos = self.query(url + "&pageToken={}".format(next_page))
                items += videos['items']
                next_page = videos["nextPageToken"] if videos["nextPageToken"] else None
        finally:
            for index, video in enumerate(items):
                items[index]['details'] = self.grab_video_details(video['id']['videoId'])
            return {"items": items}

    def grab_video_details(self, video_id: str):
        fields = "items(snippet(description),statistics(viewCount))"
        result = self.query(self.build_url("videos", fields, "", "", "", "", "snippet,statistics") + "&id=" + video_id)
        return {**result['items'][0]['statistics'], **result['items'][0]['snippet']}


class ArticleBuilder:
    @staticmethod
    def parse(file: str) -> {}:
        ret_dic = dict()
        ret_dic['slides'] = []
        with open(file) as fp:
            contents = fp.read().split('-' * 10)[0]
            for i, entry in enumerate(contents.split('-' * 5)):
                urls = re.findall(r"!\[slide\]\((.*)\)", entry)
                for u in urls:
                    entry = entry.replace("![slide]({})".format(u), "")
                entry = entry.strip().rstrip().replace("\n\n", "<br/>")
                split_content = entry.split("<br/>")
                ret_content = []
                skip_to = 0
                for index, line in enumerate(split_content):
                    if index < skip_to or line == "":
                        continue
                    elif line.startswith('```') and not line.endswith('```'):
                        skip_to = index
                        while not line.endswith('```'):
                            skip_to += 1
                            line += "\n\n"
                            line += split_content[skip_to]

                        skip_to += 1
                    ret_content.append(line)

                ret_dic['slides'].append({"images": urls, "content": ret_content})
        return ret_dic

    def process_files(self, folder: str):
        for file in os.listdir(folder):
            current = folder + "/" + file
            if os.path.isdir(current):
                self.process_files(current)
            elif os.path.isfile(current):
                dest_folder = BASE_DIR + '/../bytepen/src/json/articles'
                if not os.path.isdir(dest_folder):
                    os.mkdir(dest_folder)
                try:
                    with open(current) as fp:
                        video_id = re.findall(r"VideoID:([A-Za-z0-9-_]*)", fp.read().split('-' * 10)[1])[0]
                    with open(dest_folder + "/{}.json".format(video_id), 'w') as fp:
                        json.dump(self.parse(current), fp)
                except IndexError:
                    print("No Video ID: {}".format(current.split("articles/")[1]))
        return


do_articles = True
do_api = True
include_tests = True

if "--articlesOnly" in argv:
    do_api = False
elif "--apiOnly" in argv:
    do_articles = False

if "--prod" in argv:
    include_tests = False

if do_articles:
    ab = ArticleBuilder()
    ab.process_files(BASE_DIR + '/../bytepen/articles')

if do_api:
    yt = YouTubeGrabber()
    yt.write_json("Playlists", yt.grab_all_playlists())
    yt.write_json("LatestVideo", yt.grab_latest_video())
    yt.write_json("AllVideos", yt.grab_all_videos())
