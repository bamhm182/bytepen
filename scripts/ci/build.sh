#!/bin/bash

set -e

main() {
    setup
    retrieveJSONS
    build
}

setup() {
    BASE_DIR="$(dirname $(cd "$(dirname $( cd "$(dirname "$0")" ; pwd -P ))"; pwd -P))"

    pushd "${BASE_DIR}"
        rm -r .git
    popd

    pushd "${BASE_DIR}"/bytepen
        yarn install
    popd
}

retrieveJSONS() {
    bash "${BASE_DIR}"/scripts/retrieveJSONS.sh
}

build() {
    pushd "${BASE_DIR}"/bytepen
        yarn build
    popd
}

main "${@}"