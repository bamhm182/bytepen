#!/bin/bash

set -e

function main {
    setup
    yarnBuild
    yarnStart
    unitTests
    acceptanceTests
}

function setup {
    showBanner "Setup"

    BASE_DIR="$(dirname $( cd "$(dirname "$0")" ; pwd -P ))"
    REACT_APP_HOST=http://localhost:33506
    YARN_PID=0

    setupTestJSONs

    mkdir -p ${BASE_DIR}/tmp

    pushd ${BASE_DIR}/bytepen
        yarn install
    popd

    pushd ${BASE_DIR}/acceptance
        yarn install
    popd
}

function setupTestJSONs {
    showSubsection "Setup Test JSONs"
    rm -r ${BASE_DIR}/bytepen/src/json/* || true
    mkdir -p ${BASE_DIR}/bytepen/src/json
    cp -rp ${BASE_DIR}/bytepen/test-json/* ${BASE_DIR}/bytepen/src/json/
    if [[ -d "/c/Windows/System32" ]]; then
        python ${BASE_DIR}/scripts/buildArticlesJSON.py --articlesOnly
    else
        python3 ${BASE_DIR}/scripts/buildArticlesJSON.py --articlesOnly
    fi
}

function restoreRealJSONs {
    showSubsection "Restore Real JSONs"
    pushd ${BASE_DIR}/scripts
        bash ./retrieveJSONS.sh
    popd
}

function yarnBuild {
    if [[ "${CURRENT_ENV}" != "CI/CD" ]]; then
        showBanner "Yarn Build"
        pushd ${BASE_DIR}/bytepen
            yarn build
        popd
    fi
}

function yarnStart {
    showBanner "Yarn Start"
    pushd ${BASE_DIR}/bytepen
        PORT=33506 BROWSER=none yarn start &> ${BASE_DIR}/tmp/yarnStart.log &
        YARN_PID=$!
        testConnection "${REACT_APP_HOST}" "${YARN_PID}"
    popd
}

function unitTests {
    showBanner "Unit Tests"

    pushd ${BASE_DIR}/bytepen
        CI=true yarn test
    popd
}

function acceptanceTests {
    showBanner "Acceptance Tests"

    pushd ${BASE_DIR}/acceptance
        ELECTRON_ENABLE_LOGGING=true
        if [[ "${CURRENT_ENV}" == "CI/CD" ]]; then
            xvfb-run yarn codeceptjs run --steps
        else
            yarn codeceptjs run --steps
        fi
    popd
}

function testConnection {
    COUNTER=0
    echo "Attempting to connect to ${1} (PID: ${2})..."
    until curl -s --head --request GET ${1} | grep '200 OK' > /dev/null; do
        sleep 1
        let COUNTER+=1

        if [[ "$COUNTER" -gt 40 ]]
        then
            echo "Could not connect to ${1} (PID: ${2}) after 40 seconds. Exiting..."
            exit 1
        fi

        if [[ $(( $COUNTER % 5 )) -eq 0 ]]
        then
            echo "Attempting to connect to the app server..."
        fi
    done
}

function cleanup {
    showBanner "Cleanup"
    if [[ "${CURRENT_ENV}" == "CI/CD" ]]; then
        restoreRealJSONs
    fi

    showSubsection "Kill Yarn (${YARN_PID})"
    YARN_KILLS=$(getChildProcess ${YARN_PID})
    echo "Killing: ${YARN_PID} ${YARN_KILLS}"
    if [[ "${YARN_PID}" != "0" ]]; then
        kill -9 ${YARN_PID} ${YARN_KILLS}
    fi

    # Windows
    if [[ -d "/c/Windows/System32" ]]; then
        showBanner "Windows Cleanup"
        # Kill residual Yarn start
        taskkill //f //im node.exe

        # Don't close git bash
        if [[ "${CURRENT_ENV}" != "CI/CD" ]]; then
            echo "[PRESS ENTER TO CONTINUE]"
            read trash
        fi
    fi
}
trap cleanup EXIT

function getChildProcess() {
  parent=${1}
  child=$(ps -ef | tr -s " " | cut -d " " -f1-4 | grep -i " ${parent} " | cut -d" " -f2 | tr '\r\n' ' ' | cut -d " " -f2)
  if [[ "${child}" != "" ]]; then
    printf "${child} "
    getChildProcess ${child}
  fi
}

function showBanner {
    echo "============================="
    echo "  ${1}"
    echo "============================="
}

function showSubsection {
    echo "- - - - - - - - - - - - - - -"
    echo "  ${1}"
    echo "- - - - - - - - - - - - - - -"
}

main