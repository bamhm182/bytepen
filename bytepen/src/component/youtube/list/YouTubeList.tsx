import * as React from 'react';
import styled from 'styled-components';
import { YouTubeVideoModel } from '../models/YouTubeVideoModel';

interface State {
    width: number;
}

interface Props {
    className?: string;
    videos: Array<{}>;
    onClick: (e: any) => void;
    fullTitles: boolean;
}

export class YouTubeList extends React.Component<Props, State> {
    constructor(props: Props, state: State) {
        super(props, state);
        this.state = {width: 400};
    }

    public componentDidMount() {
        this.updateDimensions();
        window.addEventListener('resize', this.updateDimensions.bind(this));
    }

    public componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions.bind(this));
    }

    public render() {
        return (
            <div
                className={this.props.className}
                style={{"width": this.state.width}}
            >
                <div className="listTitle">Select a Video:</div>
                {
                    this.props.videos.map((v: YouTubeVideoModel, index) => {
                        return (
                            <div
                                className="listItem"
                                key={index}
                                id={v.id}
                                onClick={this.props.onClick}
                            >
                                {index + 1}: {this.props.fullTitles ? v.title : v.title.substr(v.title.indexOf(":") + 2)}
                            </div>
                        );
                    })
                }
            </div>
        );
    }

    private updateDimensions() {
        let newWidth = document.getElementsByClassName('body')[0] ?
            document.getElementsByClassName('body')[0].clientWidth :
            400;
        newWidth = newWidth > 800 ? 800 : newWidth;
        this.setState({width: newWidth});
    }
}

export const StyledYouTubeList = styled(YouTubeList)`
margin-bottom: 15px;
border-bottom: 1px solid rgba(0,0,0,0.25);
max-width: 800px;
padding: 5px 0 15px 0;

.listTitle {
margin-bottom: 20px;
}

.listItem {
  padding-left: 15px;
  margin-bottom: 15px;
  cursor: pointer;
  color: #00A7E9;
}

.selectedListItem {
  color: #000;
}
`;