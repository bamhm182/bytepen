import { YouTubeArticleModel } from '../models/YouTubeArticleModel';
import { YouTubeVideoModel } from '../models/YouTubeVideoModel';
import { YouTubeRepository } from './YouTubeRepository';
import { YouTubeSlideModel } from '../models/YouTubeSlideModel';

export class StubYouTubeRepository implements YouTubeRepository {
    public findAllVideosInPlaylist(id: string) {
        return Promise.resolve([
            new YouTubeVideoModel('abc1', 'Test Video 1'),
            new YouTubeVideoModel('abc2', 'Test Video 2'),
            new YouTubeVideoModel('abc3', 'Test Video 3'),
        ]);
    }

    public findPlaylistIdByTitle(title: string) {
        return Promise.resolve('12345');
    }

    public findLatestVideo() {
        return Promise.resolve(new YouTubeVideoModel('12345', 'Test Video'));
    }

    public findArticleByVideoID(id: string): YouTubeArticleModel {
        return new YouTubeArticleModel([new YouTubeSlideModel(['https://slide.one/url'], ['This is an example'])])
    }

}