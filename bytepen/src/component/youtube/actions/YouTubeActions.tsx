import { Repositories } from '../../../utils/Repositories';
import { Stores } from '../../../utils/Stores';
import { YouTubePlaylistModel } from '../models/YouTubePlaylistModel';
import { YouTubeVideoModel } from '../models/YouTubeVideoModel';
import { YouTubeRepository } from '../repositories/YouTubeRepository';
import { YouTubeStore } from '../stores/YouTubeStore';

export class YouTubeActions {
    private youtubeStore: YouTubeStore;
    private youtubeRepository: YouTubeRepository;

    constructor(stores: Partial<Stores>, repositories: Partial<Repositories>) {
        this.youtubeStore = stores.youtubeStore!;
        this.youtubeRepository = repositories.youtubeRepository!;
    }

    public async setupPlaylist(title: string, search: string = "") {
        const id = await this.youtubeRepository.findPlaylistIdByTitle(title);
        this.youtubeStore.setPlaylist(new YouTubePlaylistModel());
        this.youtubeStore.playlist.setId(id);
        this.youtubeStore.playlist.setTitle(title);
        this.youtubeStore.playlist.setVideos(await this.youtubeRepository.findAllVideosInPlaylist(id));
        if (search !== "") {
            const video = await this.youtubeStore.playlist.videos
                .filter((v: YouTubeVideoModel) => {
                    const currentTitle = v.title
                        .toLowerCase()
                        .replace(/\s+/g, '-')
                        .replace(/[^a-zA-Z0-9-_]/g, '');
                    const currentSearch = search.substr(search.indexOf("v=") + 2)
                        .toLowerCase()
                        .replace(/\s+/g, '-')
                        .replace(/[^a-zA-Z0-9-_]/g, '');
                    return currentTitle.endsWith(currentSearch);
                });
            this.youtubeStore.setCurrentVideo(video[0] ? video[0] : this.youtubeStore.playlist.videos[0]);
        } else {
            this.youtubeStore.setCurrentVideo(this.youtubeStore.playlist.videos[0]);
        }
        this.youtubeStore.setArticle(
            await this.youtubeRepository.findArticleByVideoID(this.youtubeStore.currentVideo.id)
        );

        if (document.getElementById(this.youtubeStore.currentVideo.id)) {
            document.getElementById(this.youtubeStore.currentVideo.id)!.classList.add('selectedListItem');
        }
    }

    public async setupLatestVideo() {
        this.youtubeStore.setCurrentVideo(await this.youtubeRepository.findLatestVideo());
        this.youtubeStore.setArticle(
            await this.youtubeRepository.findArticleByVideoID(this.youtubeStore.currentVideo.id)
        );
    }

    public async setupCurrentVideo(id: string) {
        this.youtubeStore.setCurrentVideo(await this.youtubeStore.playlist.videos
            .filter((v: YouTubeVideoModel) => v.id === id)
            .map((v: YouTubeVideoModel) => {
                return v;
            })[0]);
        this.youtubeStore.setArticle(await this.youtubeRepository.findArticleByVideoID(id));
        const title = this.youtubeStore.currentVideo.title;

        history.pushState("", "", "/" +
            this.youtubeStore.playlist.title.toLowerCase().replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-_]/g, '') +
            "?v=" +
            title.substr(title.indexOf(":") + 2).toLowerCase().replace(/\s+/g, '-').replace(/[^a-zA-Z0-9-_]/g, ''));
    }
}