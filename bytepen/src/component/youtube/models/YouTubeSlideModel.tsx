import { action, computed, observable } from 'mobx';

export class YouTubeSlideModel {
    @observable private _images: string[] = [];
    @observable private _content: string[] = [];

    constructor(images: string[] = [], content: string[] = []) {
        this._images = images;
        this._content = content;
    }

    @computed
    get images(): string[] {
        return this._images;
    }

    @computed
    get content(): string[] {
        return this._content;
    }

    @action.bound
    public setImages(images: string[]) {
        this._images = images;
    }

    @action.bound
    public setContent(content: string[]) {
        this._content = content;
    }
}