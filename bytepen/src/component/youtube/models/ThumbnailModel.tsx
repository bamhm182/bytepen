import { observable } from 'mobx';

export class ThumbnailModel {
    @observable public _url: string = '';
    @observable public _width: number = 0;
    @observable public _height: number = 0;
}