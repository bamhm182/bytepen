import { action, computed, observable } from 'mobx';
import { YouTubePlaylistItemModel } from './YouTubePlaylistItemModel';

export class YouTubeJSONModel {
    @observable private _items: YouTubePlaylistItemModel[] = [];

    @computed
    get items(): YouTubePlaylistItemModel[] {
        return this._items;
    }
}
