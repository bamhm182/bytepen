import { action, computed, observable } from 'mobx';

export class YouTubeVideoModel {
    @observable private _id: string = '';
    @observable private _title: string = '';

    constructor(id: string = '', title: string = '') {
        this._id = id;
        this._title = title;
    }

    @computed
    get id(): string {
        return this._id;
    }

    @computed
    get title(): string {
        return this._title;
    }

    @action.bound
    public setId(id: string) {
        this._id = id;
    }

    @action.bound
    public setTitle(title: string) {
        this._title = title;
    }
}