import { observable } from 'mobx';
import { ThumbnailModel } from './ThumbnailModel';

export class YouTubeSnippetModel {
    @observable public _title: string = '';
    @observable public _thumbnails: ThumbnailModel[] = [];
}