import { action, computed, observable } from 'mobx';
import { YouTubeVideoModel } from './YouTubeVideoModel';

export class YouTubePlaylistModel {
    @observable private _id: string = '';
    @observable private _title: string = '';
    @observable private _videos: YouTubeVideoModel[] = [];

    @action.bound
    public setId(value: string) {
        this._id = value;
    }

    @action.bound
    public setTitle(value: string) {
        this._title = value;
    }

    @action.bound
    public setVideos(videos: YouTubeVideoModel[]) {
        this._videos = videos;
    }

    @computed
    get id(): string {
        return this._id;
    }

    @computed
    get title(): string {
        return this._title;
    }

    @computed
    get videos(): YouTubeVideoModel[] {
        return this._videos;
    }
}