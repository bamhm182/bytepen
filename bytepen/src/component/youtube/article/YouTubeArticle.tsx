import { observer } from 'mobx-react';
import * as React from 'react';
import styled from 'styled-components';
import { YouTubeArticleModel } from '../models/YouTubeArticleModel';
import Swiper from 'swiper';
import { YouTubeSlideModel } from '../models/YouTubeSlideModel';
import 'swiper/dist/css/swiper.min.css'
import { RightArrow } from '../../icon/RightArrow';

interface State {
    width: number;
    currentSlide: number;
}

interface Props {
    className?: string;
    article: YouTubeArticleModel;
}

@observer
export class YouTubeArticle extends React.Component<Props, State> {
    private swiper: Swiper;

    constructor(props: Props, state: State) {
        super(props, state);
        this.state = {width: 400, currentSlide: 1};
    }

    public componentDidMount() {
        this.updateDimensions();
        this.initSwiper();
        window.addEventListener('resize', this.updateDimensions.bind(this));
        this.runCodePrettify();
    }

    public componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions.bind(this));
    }

    public initSwiper() {
        this.swiper = new Swiper('.swiper-container', {
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            keyboard: {
                enabled: true,
                onlyInViewport: false
            },
            on: {
                slideChange: () => {
                    this.setState({currentSlide: this.swiper.activeIndex + 1})
                }
            },
            breakpointsInverse: true,
            breakpoints: {
                625: {
                    noSwipingSelector: ".codeContainer"
                }
            }
        });
    }

    public componentWillReceiveProps(nextProps: Readonly<Props>, nextContext: any): void {
        this.updateSlides(nextProps.article.slides);
        this.runCodePrettify();
    }

    public updateSlides(slides: YouTubeSlideModel[]) {
        if (this.swiper) {
            this.swiper.removeAllSlides();
            this.swiper.appendSlide(this.createSlides(slides));
            this.setState({currentSlide: 1});
        }
    }

    public createSlides(slides: YouTubeSlideModel[]) {
        return slides.map((s: YouTubeSlideModel) => {
            const d = document.createElement('div');
            d.className = "swiper-slide";
            s.images.map((url: string) => {
                const i = document.createElement('img');
                i.src = url;
                d.appendChild(i);
                return "Done"
            });
            const c = document.createElement('div');
            c.innerHTML = s.content.join("");
            d.appendChild(c);
            return d.outerHTML;
        });
    }

    public render() {
        return (
            <div className={this.props.className + " article"} style={{"width": this.state.width}}>
                <div className={'swiper-container'}>
                    {
                        <div className={'nav'}
                             style={{"display": this.swiper && this.swiper.slides.length > 1 ? "flex" : "none"}}>
                            <div className={'swiper-button-prev'}><RightArrow/></div>
                            {
                                this.swiper &&
                                <p>Slide {this.state.currentSlide} of {this.swiper.slides.length}</p>
                            }
                            <div className={'swiper-button-next'}><RightArrow/></div>
                        </div>
                    }
                    <div className={'swiper-wrapper'}/>
                </div>
                {
                    this.props.article.slides.length === 0 &&
                    <p className={"comingSoon"}>This article is coming soon!</p>
                }
            </div>
        );
    }

    private updateDimensions() {
        let newWidth = document.getElementsByClassName('body')[0] ?
            document.getElementsByClassName('body')[0].clientWidth :
            400;
        newWidth = newWidth > 800 ? 800 : newWidth;
        this.setState({width: newWidth});
        this.swiper && this.swiper.update();
    }

    private runCodePrettify() {
        const script = document.createElement('script');
        script.type = 'text/javascript';
        script.async = true;

        script.src = 'https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(script);
    }
}

export const StyledYouTubeArticle = styled(YouTubeArticle)`
max-width: 800px;
margin-top: 20px;

.swiper-button-next, .swiper-button-prev {
  background-image: none;
  position: unset;
  width: 25px;
  height: 25px;
  margin-top: 12px;
  text-align: center;
    
  svg {
    width: 25px;
    height: 25px;
  }
}

.swiper-button-prev {
  transform: scaleX(-1);
}

.comingSoon {
  font-weight: 600;
  font-size: 20px;
}

.nav {
  display: flex;
  justify-content: space-evenly;
  align-content: center;
  width: 50%;
  margin: auto;
  min-width: 150px;
}

.swiper-slide img {
  width: 99%;
  border: solid 1px rgba(0,0,0,0.1)
}

li.L0, li.L1, li.L2, li.L3, li.L4,
li.L5, li.L6, li.L7, li.L8, li.L9 {
  list-style-type: decimal;
  background: #fff;
  padding-left: 10px;
  padding-right: 30px;
}

.codeCopy {
  position: absolute;
  right: 0;
  top: 3px;
  padding: 5px;
  display: flex;
  
  svg {
    fill: white;
    cursor: pointer;
    opacity: 0.25;
    transition: opacity .3s;
  }
}

.codeContainer {
  position: relative;
  
  .codeCopy svg:hover {
    opacity: 1;
  }
}

.codeCopiedMessage {
  opacity: 0;
  color: #FFF;
  margin: 2px 10px 0 0;
}

.flashCopiedMessage {
  animation: 3s ease 0s normal forwards 1 flashCopied;
}

@keyframes flashCopied {
  0% { opacity: 0; }
  5% { opacity: 1; }
  75% { opacity: 1; }
  100% { opacity: 0; }
}

.codeTitle {
  background: #333;
  color: white;
  border-radius: 5px 5px 0 0;
  height: 40px;
  display: flex;
  align-items: center;
  padding-left: 10px;
}

pre.prettyprint {
  min-height: 35px;
  display: flex;
  align-items: center;
  overflow: auto;
  margin-top: 0;
}

.language-shell, .language-root-shell, .language-powershell {
    ol {
      padding-inline-start: 0;
    
      li {
        list-style-type: none;
      }
      
      li::before {
        padding-right: 10px;
      }
    }
}

.language-shell > ol > li::before {
  content: "$";
}

.language-root-shell > ol > li::before {
  content: "#";
}

.language-powershell > ol > li::before {
  content: 'C:\005C>';
}

p > code {
  background: #ddd;
  padding: 1px;
}

`;