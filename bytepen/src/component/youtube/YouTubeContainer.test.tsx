import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { StyledYouTubeArticle } from './article/YouTubeArticle';
import { StyledYouTubeList } from './list/YouTubeList';
import { YouTubeArticleModel } from './models/YouTubeArticleModel';
import { YouTubeVideoModel } from './models/YouTubeVideoModel';
import { StyledYouTubeVideo } from './video/YouTubeVideo';
import { YouTubeContainer } from './YouTubeContainer';
import { YouTubeSlideModel } from './models/YouTubeSlideModel';

describe('YouTubeContainer', () => {
    let subject: ShallowWrapper;
    let youtubeStore: any;
    let youtubeActions: any;

    beforeEach(() => {
        youtubeStore = {
            setPlaylistTitle: jest.fn(),
            setCurrentVideoId: jest.fn(),
            currentVideo: new YouTubeVideoModel("123", "test"),
            article: new YouTubeArticleModel([new YouTubeSlideModel([], ["This is a test article"])])
        };
        youtubeActions = {
            setupPlaylist: jest.fn(),
            setupLatestVideo: jest.fn(),
            setupCurrentVideo: jest.fn()
        };
        subject = shallow(
            <YouTubeContainer
                youtubeStore={youtubeStore}
                youtubeActions={youtubeActions}
            />
        );
    });

    it('should not render a list of videos by default', () => {
        expect(subject.find(StyledYouTubeList).exists()).toBeFalsy();
    });

    it('should render a video', () => {
        expect(subject.find(StyledYouTubeVideo).exists()).toBeTruthy();
    });

    it('should render an article', () => {
        expect(subject.find(StyledYouTubeArticle).exists()).toBeTruthy();
    });

    it('should pass the playlist to the store', () => {
        subject = shallow(
            <YouTubeContainer
                youtubeStore={youtubeStore}
                youtubeActions={youtubeActions}
                playlist={'foo'}
                search={'bar'}
            />
        );
        expect(youtubeActions.setupPlaylist).toHaveBeenCalledWith('foo', 'bar');
    });

    it('should setup latest video if not a playlist', () => {
        subject = shallow(
            <YouTubeContainer
                youtubeStore={youtubeStore}
                youtubeActions={youtubeActions}
            />
        );
        expect(youtubeActions.setupLatestVideo).toHaveBeenCalled();
    });

    it('should update the store when a list link is clicked', async () => {
        await (subject.instance() as YouTubeContainer).listItemClicked({target: {id: 'foo'}});
        expect(youtubeActions.setupCurrentVideo).toHaveBeenCalledWith('foo');
    });
});