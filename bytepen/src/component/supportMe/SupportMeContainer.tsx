import * as React from 'react';
import styled from 'styled-components';
import SupportMeModules from './SupportMe.json';
import { StyledSupportMeModule } from './SupportMeModule';

interface Props {
    className?: string;
}

export class SupportMeContainer extends React.Component<Props> {

    public render() {
        return (
            <div
                className={this.props.className}
            >
                Running a YouTube channel that provides weekly content takes a lot of work. I do it because I really
                enjoy sharing knowledge that I've acquired over the years. I also want to work toward making YouTube a
                better place to quickly get the answers you're looking for without having to sit through a 20 minute
                video for the 1 minute answer you're looking for. With that said, it makes it easier to justify spending
                all this time and money providing the highest quality content I can if I know people care about my
                mission. If you want to show your appreciation and support for what I'm doing, here are a few ways to do
                so. Most of these are completely free for you!<br/><br/>

                <div className="title">Free</div>
                <div
                    className="modules"
                >
                    {
                        SupportMeModules.free.map((mod, index) => {
                            return (
                                <StyledSupportMeModule
                                    key={index}
                                    image={mod.image}
                                    title={mod.title}
                                    description={mod.description}
                                    href={mod.href}
                                />
                            )
                        })
                    }
                </div>


                <div className="title">Paid</div><br/>
                I understand that parting with your hard earned money is not something to be taken lightly, but it helps
                a ton if that's the way you choose to show your support. You can do so in the following ways.<br/><br/>
                <div
                    className="modules"
                >
                {
                    SupportMeModules.paid.map((mod, index) => {
                        return (
                            <StyledSupportMeModule
                                key={index}
                                image={mod.image}
                                title={mod.title}
                                description={mod.description}
                                href={mod.href}
                            />
                        )
                    })
                }
                </div>
                Whether you directly support my efforts through any of these means or not, I really appreciate the time
                you've spent to enjoy the content I'm providing.<br/><br/><strong>Thank you!</strong><br/><br/>- Dylan

            </div>
        );
    }
}

export const StyledSupportMeContainer = styled(SupportMeContainer)`
padding-bottom: 30px;
.modules {
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
}

.title {
  font-weight: bold;
  font-size: 20px;
}


`;