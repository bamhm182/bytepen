import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { SupportMeContainer } from './SupportMeContainer';
import { StyledSupportMeModule } from './SupportMeModule';

describe('SupportMeContainer', () => {
    let subject: ShallowWrapper;
    beforeEach(() => {
        subject = shallow(<SupportMeContainer/>);
    });

    it('should contain a support me module', () => {
        expect(subject.find(StyledSupportMeModule).length).toBe(7);
    });
});