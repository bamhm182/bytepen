import * as React from 'react';

export const RightArrow = () => {
    const pathD1 = "M500,9.9C229.4,9.9,10,229.4,10,500c0,270.5,219.4,490,490,490c270.7,0,490-219.5,490-490C990," +
        "229.4,770.7,9.9,500,9.9z M500,939.9c-242.9,0-439.8-197-439.8-439.8C60.2,257,257.1,60.2,500,60.2c243,0," +
        "439.9,196.9,439.9,439.9C939.9,742.9,742.9,939.9,500,939.9z";
    const pathD2 = "M823.9,511.6L603.3,738.4c0,0-26.1,27.6-26.1-2.4c0-30.1,0-102.8,0-102.8s-17.7,0-44.8,0c-77.6," +
        "0-218.6,0-276.2,0c0,0-15.6,4-15.6-19.6c0-23.7,0-216.1,0-233.1c0-16.9,13.1-16.6,13.1-16.6c55.9,0,201.8,0," +
        "276.8,0c24.1,0,40.2,0,40.2,0s0-58.3,0-94.9c0-36.5,27.3-9,27.3-9s206,199.5,227.6,221.1C841.4,496.7,823.9," +
        "511.6,823.9,511.6z";

    return (
        <svg version="1.1" x="0px" y="0px" viewBox="0 0 1000 1000">
            <path d={pathD1}/>
            <path d={pathD2}/>
        </svg>
    );
};