import React from 'react';

export const DownCarrot = ({
                               style = {},
                               fill = '#fff',
                               width = '100%',
                               className = '',
                               height = '100%',
                               viewBox = '0 0 1000 1000',
                           }) =>
    <svg
        width={width}
        style={style}
        height={height}
        viewBox={viewBox}
        className={className}
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
    >
        <path d="M10,292l488.8,489l1.1-1.2l1.2,1.2L990,292l-73-73L500,636L83,219L10,292z" fill={fill}/>
    </svg>;

