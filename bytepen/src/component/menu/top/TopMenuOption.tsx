import { action } from 'mobx';
import * as React from 'react';
import styled from 'styled-components';

interface Props {
    className?: string;
    title: string;
    href: string;
}

export class TopMenuOption extends React.Component<Props> {
    public componentDidMount() {
        if (window.location.pathname === this.props.href) {
            if (document.getElementById(this.props.title)) {
                document.getElementById(this.props.title)!.classList.add('selected');
            }
        }
    }

    @action.bound
    public showMenu() {
        if (this.props.children) {
            document.querySelector(".dropdown." + this.props.title.split(" ").join(""))!.classList.add("visible");
        }
    }

    @action.bound
    public hideMenu() {
        if (this.props.children) {
            document.querySelector(".dropdown." + this.props.title.split(" ").join(""))!.classList.remove("visible");
        }
    }

    public render() {
        return (
            <div
                className={this.props.className}
                onMouseOver={this.showMenu}
                onMouseOut={this.hideMenu}
            >
                <a
                    href={this.props.href}
                    className="menuLink"
                >
                    <div className="menuOption" id={this.props.title.split(' ').join('')}>{this.props.title}</div>
                </a>
                {
                    this.props.children &&
                    <div
                        className={"dropdown " + this.props.title.split(' ').join('')}
                    >
                        {this.props.children}
                    </div>
                }
            </div>
        );
    }
}

export const StyledTopMenuOption = styled(TopMenuOption)`
max-width: 150px;
display: flex;
justify-content: center;

.dd-option {
  max-width: unset;
  display: block;
  
}

.menuLink {
    text-decoration: none;
    text-align: center;
    color: #AAAAAA;
    font-size: 25px;
    line-height: 20px;
    top: 10px;
    position: relative;
}

.dropdown {
  background: rgba(5, 39, 45, 0.95);
  padding-bottom: 20px;
  position: absolute;
  display: none;
  padding-top: 10px;
  margin-top: 5px;
  top: 50px;
}

.visible {
  display: block;
}

.menuOption {
    margin: 5px;
    border-radius: 10px;
    padding: 10px;
    white-space: nowrap;
    
    :hover {
      background: rgba(75,75,75,0.5);
      color: #EEEEEE;
    } 
}

.selected {
    background: rgba(75,75,75,0.5);
    color: #EEEEEE;
}
`;