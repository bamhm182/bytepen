import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { TopMenuContainer } from './TopMenuContainer';
import { StyledTopMenuOption } from './TopMenuOption';

describe('TopMenuContainer', () => {
    let subject: ShallowWrapper;
    beforeEach(() => {
        subject = shallow(<TopMenuContainer/>);
    });

    it('should contain menu options', () => {
        expect(subject.find(StyledTopMenuOption).length).toBe(5);
    });

    it('should contain a dropdown menu option', () => {
        expect(subject.find(StyledTopMenuOption).at(1).html()).toContain('Tutorials');
        expect(subject.find(StyledTopMenuOption).at(1).childAt(0).html()).toContain('My Important Playlist');
    });
});