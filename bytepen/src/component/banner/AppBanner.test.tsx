import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { StyledSideMenuContainer } from '../menu/side/SideMenuContainer';
import { StyledTopMenuContainer } from '../menu/top/TopMenuContainer';
import { AppBanner } from './AppBanner';

describe('AppBanner', () => {
    let subject: ShallowWrapper;
    beforeEach(() => {
        subject = shallow(<AppBanner/>);
    });

    it('should contain a title', () => {
        expect(subject.find('.title .text').text()).toBe('BytePen Studios');
    });

    it('should contain a menu', () => {
        expect(subject.find(StyledTopMenuContainer).exists()).toBeTruthy();
    });

    it('should contain a side menu', () => {
        expect(subject.find(StyledSideMenuContainer).exists()).toBeTruthy();
    });
});