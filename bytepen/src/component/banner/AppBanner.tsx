import * as React from 'react';
import styled from 'styled-components';
import { BytePenLogo } from '../icon/BytePenLogo';
import { StyledSideMenuContainer } from '../menu/side/SideMenuContainer';
import { StyledTopMenuContainer } from '../menu/top/TopMenuContainer';

interface Props {
    className?: string;
}

export class AppBanner extends React.Component<Props> {
    public render() {
        return (
            <header
                className={this.props.className}
            >
                <StyledSideMenuContainer/>
                <div className="contents">
                    <div className="title" onClick={() => {window.location.href = '/'}}>
                        <span className="img"><BytePenLogo/></span>
                        <span className="text">BytePen Studios</span>
                    </div>
                    <div className="menu">
                        <StyledTopMenuContainer/>
                    </div>
                </div>
                <div className="dropshadow"/>
            </header>
        );
    }
}

export const StyledAppBanner = styled(AppBanner)`
width: 100%;
line-height: 73px;
background: #05272D;
display: inline-block;
justify-content: left;
box-shadow: 0 30px 30px -30px #387e87;
min-width: 410px;
margin-bottom: 20px;
z-index: 10;

a {
  text-decoration: none;
}

.contents {
    width: 100%;
    line-height: 64px;
    height: 73px;
    color: #FFFFFF;
    font-family: Licencia;
    font-size: 45px;
    display: flex;
    justify-content: left;
}

.title {
  width: 20%;
  min-width: 250px;
  padding-left: 50px;
  cursor: pointer;
}

.menu {
  position: absolute;
  left: 300px;
}

.dropshadow {
    width: 100%;
    height: 1px;
    background-image: linear-gradient(to right,#0a4f5b,#54acb8,#0a4f5b);
    border-radius: 75%;
}

.img img {
    position: relative;
    width: 30px;
    padding-right: 10px;
}

@media only screen and (max-width: 625px) {
  justify-content: center;
  .title {
    padding-left: 0px;
  }
  .contents {
    justify-content: center;
  }
  .menu {
    width: 0px;
  }
}
`;