import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { VideoLengthCheckerContainer } from './VideoLengthCheckerContainer';
import { StyledVideoLengthModule } from './VideoLengthModule';

describe('VideoLengthCheckerContainer', () => {
    let subject: ShallowWrapper;
    beforeEach(() => {
        subject = shallow(<VideoLengthCheckerContainer/>);
    });

    it('should contain three section textareas with titles', () => {
        expect(subject.find(StyledVideoLengthModule).length).toBe(4);
    });
});