import * as React from 'react';
import styled from 'styled-components';
import { StyledVideoLengthModule } from './VideoLengthModule';
interface Props {
    className?: string;
}

export class VideoLengthCheckerContainer extends React.Component<Props> {
    public render() {
        return (
            <div
                className={this.props.className}
            >
                <StyledVideoLengthModule
                    title="Intro"
                    maxSeconds={4}
                />
                <StyledVideoLengthModule
                    title="Setup"
                    maxSeconds={10}
                />
                <StyledVideoLengthModule
                    title="Content"
                    maxSeconds={60}
                />
                <StyledVideoLengthModule
                    title="Outro"
                    maxSeconds={4}
                />
            </div>
        );
    }
}

export const StyledVideoLengthCheckerContainer = styled(VideoLengthCheckerContainer)`

display: block;
.videoSection {
  width: 500px;
}
`;