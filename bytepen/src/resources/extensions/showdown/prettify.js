import Icon from '@mdi/react';
import {mdiContentCopy} from '@mdi/js';
import * as React from 'react';
import showdown from 'showdown';

//
//  Google Prettify
//  A showdown extension to add Google Prettify (http://code.google.com/p/google-code-prettify/)
//  hints to showdown's HTML output.
//

(function (extension) {
    'use strict';

    if (typeof showdown !== 'undefined') {
        // global (browser or nodejs global)
        extension(showdown);
    } else if (typeof define === 'function' && define.amd) {
        // AMD
        define(['showdown'], extension);
    } else if (typeof exports === 'object') {
        // Node, CommonJS-like
        module.exports = extension(require('showdown'));
    } else {
        // showdown was not found so we throw
        throw Error('Could not find showdown library');
    }

}(function (showdown) {
    'use strict';

    let counter = 0;

    showdown.extension('prettify', function () {
        return [{
            type: 'output',
            filter: function (source) {
                let copyCode = source.replace(/<pre[^>]*>?[\n\s]?<code[^>]*>/gi, '');
                copyCode = copyCode.replace(/<\/code>?[\n\s]?<\/pre>/gi, '');
                copyCode = copyCode.trim();
                copyCode = '<div style="display:none" class="copyCode' + counter + '">' + copyCode + "</div>";


                const copyToClipboard = '\
                    const el = document.createElement(\'textarea\'); \
                    const el2 = document.getElementsByClassName(\'copyCode' + counter + '\')[0];\
                    el.value = el2.innerHTML; \
                    document.body.appendChild(el); \
                    el.select(); \
                    document.execCommand(\'copy\'); \
                    document.body.removeChild(el);\
                    document.getElementsByClassName(\'codeCopiedMessage' + counter + '\')[0].classList.remove(\'flashCopiedMessage\');\
                    document.getElementsByClassName(\'codeCopiedMessage' + counter + '\')[0].offsetWidth;\
                    document.getElementsByClassName(\'codeCopiedMessage' + counter + '\')[0].classList.add(\'flashCopiedMessage\');\
                    ';

                const copyObj = <Icon path={mdiContentCopy}/>;
                const copyIcon = '<div class="codeCopy"><div class="codeCopiedMessage' + counter + ' codeCopiedMessage">Saved to Clipboard</div><svg width="25" height="25" onclick="' + copyToClipboard + '"><path d="' + copyObj.props.path + '"/></svg></div>';
                const generateTitle = (codeClass) => {
                    switch (codeClass.replace('class="', '').split(" ")[1]) {
                        case "bash":
                            return "Bash";
                        case "js":
                            return "JavaScript";
                        case "powershell":
                            return "PowerShell";
                        case "shell":
                            return "Shell";
                        case "root-shell":
                            return "Shell (root)";
                        default:
                            return "Code";
                    }
                };
                const guts = source.replace(/(<pre[^>]*>)?[\n\s]?<code([^>]*)>/gi, function (match, pre, codeClass) {
                    if (pre) {
                        const preClass = codeClass.slice(0, -1) + ' prettyprint linenums"';
                        return '<div class="codeTitle">' + generateTitle(codeClass) + '</div><pre ' + preClass + '><code' + codeClass + '>';
                    } else {
                        return ' <code class="prettyprint">';
                    }
                });
                counter++;
                if (source.includes("<pre") && source.includes("<code")) {
                    return '<div class="codeContainer">' + copyIcon + copyCode + guts + '</div>'
                } else {
                    return source
                }
            }
        }];
    });
}));
