import * as React from 'react';
import logo from './Instagram.png';

export const InstagramLogo = () => {
    return (
        <img src={logo}/>
    );
};