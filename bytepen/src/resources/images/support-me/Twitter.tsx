import * as React from 'react';
import logo from './Twitter.png';

export const TwitterLogo = () => {
    return (
        <img src={logo}/>
    );
};