import { StubYouTubeRepository } from '../component/youtube/repositories/StubYouTubeRepository';
import { WebYouTubeRepository } from '../component/youtube/repositories/WebYouTubeRepository';
import { YouTubeRepository } from '../component/youtube/repositories/YouTubeRepository';

export interface Repositories {
    youtubeRepository: YouTubeRepository;
}

export const WebRepositories: Repositories = Object.freeze({
    youtubeRepository: new WebYouTubeRepository()
});

export const StubRepositories: Repositories = {
    youtubeRepository: new StubYouTubeRepository()
};