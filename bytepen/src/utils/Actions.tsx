import { YouTubeActions } from '../component/youtube/actions/YouTubeActions';
import { WebRepositories } from './Repositories';
import { stores } from './Stores';

const youtubeActions = new YouTubeActions(stores, WebRepositories);

export const actions = {
    youtubeActions
};