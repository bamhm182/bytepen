import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { StyledYouTubeContainer } from '../component/youtube/YouTubeContainer';
import { PlaylistPage } from './PlaylistPage';

describe('PlaylistPage', () => {
    let subject: ShallowWrapper;
    beforeEach(() => {
        subject = shallow(
            <PlaylistPage
                playlist="Test Playlist Title"
                location={{search: "Test Video"}}
            />
        );
    });

    it('should render a YouTubeContainer', () => {
        expect(subject.find(StyledYouTubeContainer).exists()).toBeTruthy();
        expect(subject.find(StyledYouTubeContainer).props().playlist).toBe("Test Playlist Title");
    });
});