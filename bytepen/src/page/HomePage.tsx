import * as React from 'react';
import styled from 'styled-components';
import { StyledYouTubeContainer } from '../component/youtube/YouTubeContainer';

interface Props {
    className?: string;
}

export class HomePage extends React.Component<Props> {
    public render() {
        return (
            <div
                className={this.props.className}
            >
                <span
                    className="body"
                >
                    <p className="welcomeMessage">
                        Welcome to BytePen Studios!<br/>Check out our latest video below, then continue on to
                        other episodes through the menu!<br/>I hope you find what you're looking for!
                    </p>
                    <StyledYouTubeContainer/>
                </span>
            </div>
        );
    }
}

export const StyledHomePage = styled(HomePage)`
width: 100%;
display: flex;
justify-content: center;

.welcomeMessage {
  width: 100%;
  text-align: center;
}

.body {
width: 85%;
justify-content: center;
display: flex;
flex-wrap: wrap;
}
`;