import * as React from 'react';
import styled from 'styled-components';
import { StyledYouTubeContainer } from '../component/youtube/YouTubeContainer';

interface Props {
    className?: string;
    playlist?: string;
    location?: any;
}

export class PlaylistPage extends React.Component<Props> {
    public render() {
        return (
            <div
                className={this.props.className}
            >
                <span
                    className="body"
                >
                    <StyledYouTubeContainer
                        playlist={this.props.playlist}
                        search={this.props.location.search}
                    />
                </span>
            </div>
        );
    }
}

export const StyledPlaylistPage = styled(PlaylistPage)`
width: 100%;
display: flex;
justify-content: center;

.body {
width: 85%;
justify-content: center;
display: flex;
}
`;