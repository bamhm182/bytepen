import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { TutorialsPage } from './TutorialsPage';
import Playlists from '../json/Playlists.json';

describe('TutorialsPage', () => {
    let subject: ShallowWrapper;
    beforeEach(() => {
        subject = shallow(<TutorialsPage/>);
    });

    it('should render a links based on the Playlists.json, excluding specific ones', () => {
        expect(subject.find('a').length).toBe(Playlists.items.length-1);
    });

    it('should render an icon for each video, excluding specific ones', () => {
        expect(subject.find('img').length).toBe(Playlists.items.length-1);
    });
});