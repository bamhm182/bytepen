import * as React from 'react';
import styled from 'styled-components';
import Playlists from '../json/Playlists.json'

interface Props {
    className?: string;
}

export class TutorialsPage extends React.Component<Props> {
    public render() {
        return (
            <div
                className={this.props.className}
            >
                <div
                    className="body"
                >
                    {
                        Playlists.items.filter((p: any, i: number) => {
                            // Skip Projects playlist
                            if (p.snippet.title !== "Projects") {
                                return p;
                            }
                        }).sort((a,b) => (a.snippet.title > b.snippet.title) ? 1 : -1).map((p: any, i: number) => {
                            const videos = require("../json/playlists/" + p.id + ".json");
                            return (
                                <div className="playlistCard" key={i}>
                                    <a href={
                                        "/" + p.snippet.title
                                            .toLowerCase()
                                            .replace(/\s+/g, '-')
                                            .replace(/[^a-zA-Z0-9-_]/g, '')
                                    }>
                                        <img src={videos.items[0].snippet.thumbnails.medium.url}/><br/>
                                        <span>{p.snippet.title}</span>
                                    </a>
                                </div>
                            );
                        })
                    }
                </div>
            </div>
        );
    }
}

export const StyledTutorialsPage = styled(TutorialsPage)`
width: 100%;
display: flex;
justify-content: center;

.body {
  width: 85%;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
}

.playlistCard {
  display: flex;
  justify-content: center;
  padding: 15px;
  text-align: center;
}
`;