import * as React from 'react';
import styled from 'styled-components';
import { StyledVideoLengthCheckerContainer } from '../component/admin/VideoLengthChecker/VideoLengthCheckerContainer';

interface Props {
    className?: string;
}

export class VideoLengthCheckerPage extends React.Component<Props> {
    public render() {
        return (
            <div
                className={this.props.className}
            >
                <div
                    className="body"
                >
                    <StyledVideoLengthCheckerContainer/>
                </div>
            </div>
        );
    }
}

export const StyledVideoLengthCheckerPage = styled(VideoLengthCheckerPage)`
width: 100%;
height: 1150px;
display: flex;
justify-content: center;

.body {
  width: 85%;
  min-width: 500px;
}
`;