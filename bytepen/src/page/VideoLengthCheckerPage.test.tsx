import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { StyledVideoLengthCheckerContainer } from '../component/admin/VideoLengthChecker/VideoLengthCheckerContainer';
import { VideoLengthCheckerPage } from './VideoLengthCheckerPage';

describe('VideoLengthCheckerPage', () => {
    let subject: ShallowWrapper;
    beforeEach(() => {
        subject = shallow(<VideoLengthCheckerPage/>);
    });

    it('should render a video length checker container', () => {
        expect(subject.find(StyledVideoLengthCheckerContainer).exists()).toBeTruthy();
    });
});