import * as React from 'react';
import styled from 'styled-components';
import { StyledSupportMeContainer } from '../component/supportMe/SupportMeContainer';

interface Props {
    className?: string;
}

export class SupportMePage extends React.Component<Props> {
    public render() {
        return (
            <div
                className={this.props.className}
            >
                <div
                    className="body"
                >
                <StyledSupportMeContainer/>
                </div>
            </div>
        );
    }
}

export const StyledSupportMePage = styled(SupportMePage)`
width: 100%;
display: flex;
justify-content: center;

.body {
  width: 85%;
}
`;