import * as React from 'react';
import { withRouter } from 'react-router';
import styled from 'styled-components';
import { StyledAppBanner } from './component/banner/AppBanner';
import { Routes } from './Routes';
import { StyledAppFooter } from './component/banner/AppFooter';

export const WrappedRoutes = withRouter(Routes as any);

interface Props {
    className?: string;
}

export class App extends React.Component<Props> {
    public render() {
        return (
            <div
                className={this.props.className}
            >
                <StyledAppBanner/>
                <WrappedRoutes/>
                <StyledAppFooter/>
            </div>
        );
    }
}

export const StyledApp = styled(App)`
width: 100%;
height: 100%;
position: relative;
display: flex;
flex-wrap: wrap;
flex-flow: column;
`;