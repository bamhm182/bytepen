![slide](https://lh3.googleusercontent.com/wVJZjhXwgfEQQdYOAR95m1mJ94uAE1IaVQieyYVN7P5qRjoJ8Vby_hpfCamk5i2k-HH821Y1G1rbp-SZQL5fmddRIqRLQhU6z9ezKcjH_AoNntONhLePMzPLli3i-g573-YhpqGOrbrpfFmjyDhm9E5OX3az1xUrpBu-sZQDnvjABXLey3z5_vjb-M71DBRnbdXKaftNp8TFJqTbn9kqIkldpVJHcqFCzW0kCwnJVCMWc0JseVf-YqwIpEi-g-eo9jyTdF_4NyLJPCIR1VHzT8fftA6NCWdQmBxFus5iXhV9XgfhlKjjFvPWsXsxpDYUkdxRMxhyW895bFUWL3L0RhM2_-8RQJs0V8LgUOXP5WM6DNzJofPg-3pRTCQuIYbwk_P8pvxhgtJHtg7uQFM8fgy0Et1cSAtrW-iIRH8X27a-J5S_NRUiR9dHNJFVjbm5OsDRWcSjal7h2UibmuvH532lnY9ZwJMhUPwtAL6krdu6abIoETvtEP-NPxJeT9TebwYC2g-kxYbSzivY-FMQqrQPqKVDBJEPTENf83oAVkTpAmAcbVA8Gl6G9eoVjS1w9gUmNq7EWJjrRzclJwNOhdQI2FzZPaLvQRZzgL0St8mGEm6_cOLvSK4albRVUXX31LZBrTFM_iq2dgPaZfhJFn_VXXVbCxg=w640-h360-no)

In this tutorial, we will install fail2ban. We will then do some minimal configuration to ban IP addresses that are
making invalid SSH attempts. After that, we will make some invalid SSH attempts ourselves to become banned and learn
how to see the status of fail2ban and unban an IP.

-----

![slide](https://lh3.googleusercontent.com/9-wozroDFC8I9LDCow5Jji4DHpF2RnLf7jV4nJ5P5yokPPStHbRIwwevfmi9diltRsOSW_QyFIosswFb3A1kVC9D2KcL7oYtMnb2oQDgY1NGw6K43P4K9ep60d3jUDQNhni1FY7x0t2FXBdJXLu_9jwVM3lPSH5m1Iuh86MY7V2iQCFNu8XTl2YcnMxSohMJx1mhK2do2Dk7qQGZwRknUyNMVw7z_DtSoqVnvMtzTHX8vDuCFKqUkcZ3WcCG04GCXHGd1Gj0IOp5FGXUYnMmbu8jtWSPWDH2gtA7EdKFUMWFfVveIrBLdRCw0v7pukQf-xiJpc9L72BosXY-XVOcZukPQ0sOVvhQknC-J3Q7h3TDyaaf7R9xZtLH5M9i7j5RaFo5O9o-0wWgD-SstkKtj3M45mDWO2X24IwLf0J-1VO00IZevm37f3LTyWzz2FuLXFZ47foIzzxLpCwU4X5ON8-g59abh7L52T9-g4ZgCe1alv_CDE2qW66JIZphuNAjrJjGB2Mr4QbMFtL91rTr__qU3QZClpXZ24Yh9CW5x86KX3vD6IyWQ_oGKYPB6SSH2np5Z--6iQXoQsu9B2GokBA7WkIoHF3ghcVO9FcE0CF8Ui_8VdbXX4Cs28PbnTQojthojH95tdd0k-ncedfPjFXv1JEoUO8=w640-h360-no)

For this, you will need a Raspberry Pi with any OS installed and internet access.

Ideally, you should also have direct access (keyboard and monitor) to your Raspberry Pi so that when we ban ourselves
later in the tutorial, we can easily unban ourselves. I chose to do everything over SSH and changed my IP address after
getting banned instead.

-----
![slide](https://lh3.googleusercontent.com/CT2E4ib0y1bkJQdXIeo9apO_BplxQ_72RoVTkJf0b_ERjhWeC9xnBX3A65fANm-RO3sOXl_MeaGaTnQMvepXRCV2EnIR2eh86xCen6RixWfFQROV9n0RVd39VptvinFPawlsCmIOA8hMj4ewMrR_S11LUomldfkXEa5CEv-EATWRoJkZ1X1iDzuqJDUjzKODyxYwA4PbrRZF9kzQZ3aTlvEUDhzbjJCFCAWNyxFmXxMRUl9dGTIwjDLccUsnWaaa_EzQ1ojdYCkf25TA0CFi2fVnVKcTWpFMu1ALP1LB7Z9AR8nBzaUc5YESxHD23zf_a4U15XmhMDN2OdIYx8aFzD0OQ3Oa7Dh8BTZGvQQhp8hhqWe9ONzOaRuVMkExBeNkRD93S8oXAEzBSoHuG6Q56dCIR4imdJs4EHqD-H6gYwaiBOuddiu3Kk1Lyi2jMadVE-a5ibK_1PuNy7N6-nJCXj1aiQ-2ps6lw4lQ1ak8P_77962whZR7RvEGquxmB0iyvtUNIfqEd49qwwRFl_VUYELE4DLujrSIcLjghjFZGmwHChP230cqeahU9gKAiwhKeCdeLVVX2ErUF0fKXctcWCJ-_X5pT3cOBbnzQ0tzhrty9hCRJliXjgyjDkXMHmugqsyKYKFSjIVgEKUXjgRp_mkzSlNkXXY=w640-h360-no)

fail2ban can be used for a wide variety of services, from ssh to invalid wordpress logins and everything in between.
Essentially, any IP that is written into a log can have a rule written for it that will lead to it being banned.

For simplicity sake, I chose a very common one that people use; ssh.

-----
![slide](https://lh3.googleusercontent.com/iZyOv5f1VnJ03YJ4bBGBE4Bm3turQ_hTf0aIk6F57suGLScrDT-yLImgGQQCyambxc5lhxrelc5boZ63ZBfpHW6osYsC-kXPIiubgw0Jh4Qyl0DDEZhNLgD6wEuO6hGmNRrpWvN84akFW0flGwSSWH-FGcMfFzP-ivkkm3K3Vyyi4HIS09dftSsdzCjdldm5N1GfPN0uoZsLAJbv1lkiifz7GzuLXYdH4eih3lKYIJnRGI5Z9d8tzz1qAE1PUz8HOhm3l713F2ZKTT41YKYrYBrXgmcZVlvTMS1hze-W7Ta7D_Wvhl2kT2LbMXrmIP1kjkNacdEeuAz_VujqrwXlZ6uc1R_I9femRgn0d6ofmSZuTkFoAz7Ykn0Q7qWCf9mjsXWrn-iXaDB6yNKvCmSEPtVY2iHC7UiyRcWcy7u4LZgW12l4FahqvqgFwxR-kJMcAZOcUKMPsyb1WKVImGDgAP4a_vz9RDnbXBLdWc0e9V-PL2xZG7CvzW2LvpdFDdnPIXYfmglLFTTuPHttP-FkpUXegQKJh3lexNOhnmApg_pPPVE3SSOiJnVByrumDuU56Yfw0AKr6Kf1idk4VLpU9h9a-nO6LHc8ReGT1FNk-fMkGJmymaDmAq9q5aUJgMj-qX6aMHcFOf4za6sC3V1LJKRThjgyeEI=w640-h360-no)

Note that if you followed my other tutorial and set up SSH certificates and disabled PasswordAuthentication, you will
need to re-enable it with the following steps.

```shell
sudo nano /etc/ssh/sshd_config
```

Find the following line:

```text
PasswordAuthentication no
```

Change it to:

```text
#PasswordAuthentication no
```

Restart ssh with:

```shell
sudo systemctl restart ssh
```

-----
![slide](https://lh3.googleusercontent.com/Kbqm3B6rpCFY5iN8XXCorTPtg1qVO9wCXmQxip3xH26AfFCj0htjFD9zGgmy4J6DDllHxbYdL8ShdumJAd-rDPj5R57plObt0bTzw0jNBo-pGC_hf8wkcOaW2b5zOwnvyZ6MAjrtlM_w7uklBKMsWRIglZL1zNCEo6PpUd8fllfV-fHzizgv6zBvYG1k6kX8f32ES22EXBrIfEdsWIENi8VxvDaRar2CaXz1SKSdBLOPVG3Eh3vj-jSE37v8HWAiH786WBDtvcBhZYvF1AtADOaWlpPXx9SUgxKlKqiCOu8iRM6tSQ0xjOX8bWTwIatU29Tbar9913OoaF7irrxNZCqGdQpL87cxCOQpdoeRXNf68FK0f5uH5CZaqCeTDkprH3hplEYjjymlsYk2zOWNPYVX5Sr8N4lc9OwHc2KSfW4Og9e-I7PwoMA9lhVbcTw5ONX2jPcxtwEKLaecra23QzdYxtwuBbk5Mas9WGkIh54B0P8bjK7lXvcgLB89OLGJBYVg5KavQr3hXPnrcO3gooP-I52RFK8gWKYAznAMl1BV3zBd0ZASkTYODTgfGAquDaA_cAImDGfai6lHUb3iLE-feYo1qt07EXTM7ElTX9hRvhPje0saIwxDgrwWmv2MPyk5Fw6NM3MwnZGx-8qygyZqXaKNqOQ=w640-h360-no)

Now we should update our pi and install fail2ban.

```shell
sudo apt update
sudo apt install -y fail2ban
```

If your Raspberry Pi tells you you have updates to install after you run apt update, I recommend you install them with
the following command.

```shell
sudo apt upgrade
```


-----
![slide](https://lh3.googleusercontent.com/81K0s_njwBJShaXiSYz1GsDkBM8qyEeNnm56bvhr1EIquwogv2BepT1SLx_UN-Z7GnDZN2j-gsSNTNO6ZwHsCerB7DImp7lakQB0wFgCbIX8nSsdu31gFuxMY2LkGpYjcVkwOdu7oiKHS6l-WAxSUgYs_O2vJmpPXxtasCHljRYpjNK_4SxofyqakHD0j3kKkDsj_tf90l7BgzS6m0aGkVnKlDGscjHa1h3fU2W01lL4LKHhn6xY0lhUokNm1CVLa9uslFB2g5mU2kmVK47QMYitiX36kHW75m2VxmSdtjfU0er2UcgnY7AfmS6HnRr3Md8rAlNxNurbRW3rP3gRGbl9iSupidrDGxoTRWVmK4iZedHw8ZyJZMPsJyzc34mYMjPWw2o0S_Ffj5RxmCvzJD9IfLro9CFWcfZeVbB51090xjUf6n1T-a0hlc-bAd50utd4cclH1YNhmXejswKDEhC5Ug4YblRWCRn6rFA0SBbIgxdimO7IjOJMTRDuJgiZcIHUnAbCzhY3PQ3sIgycSPm6dS0QqACHyQWa7YTmnFsjaSowGtp3190LdQfzKZFFZx9NFSINIRM-M_oqe2oO9aQuF04w9yGjW-5pWohLjMZREzia5ez6xoVL0V_f87eHL7H_pmW1W7k5zP6mR0TKRyjdBEeGBdw=w640-h360-no)

Now that fail2ban is installed, I recommend you look around its files in /etc/fail2ban. You can use the cat command to
read through the jail.conf and see what a config file looks like. Most importantly, you should read the header for this
file and note that you should not modify it. Instead, you should either create a jail.local file in /etc/fail2ban or any
number of *.conf files in /etc/fail2ban/jail.d

```shell
cd /etc/fail2ban
ls
cat jail.conf
```

-----
![slide](https://lh3.googleusercontent.com/pCguuSPEE-TS7m-j_qPh-IQQ4uPMeAImklxxbxsvhM14eVC17qsxyZgtknlec9BcUltaWb6xBuWJ9oLGtM9ulU5nsI03F-K66dvZnf18EBYpN3Lx2LSDXVJeEFgzkjn2t8y51vuzYpfadeGfOUimjV3xu39txJvr8wVzWMNCKuDJc56oXuXdxl20fjGl17rxd9tRxAkEL6BOxqzp-NVPvHDwq133UuBazphtFDtTFB4SXOCglK7kLotTr0wCDLL3UEUogGGIla4BukS-bT9AQQf14nPuEBEd0Tt4iE-jqwaYNG9Nry3veqrkjo_1O1Xqr2cDv2DIbvleydxKRgn1RW-nd85GR5BiJKbjaos-0Ix4QRYkA1EUGYTEAGZrD_4rys0OZNUH7PRE_5JQD0O4LYzMnby4lSmHI6GBG2N9ohFlMo5wdvinnecZCn5rQO7tCRmnDTeg5tlLxxpx8yhq2Lfns42hvgdQJWY1y1XBZQ00JxUYj0zOMyG39rqcz-xMzJ7geGySDD3vaQ0j-B3B6r6Hxfmhk06gF8qW4sByaP58dkB0PHlNfvT2xRuVb0OM480pIFZobjWSX8O1n_UsR7--lI2MnKakgmiY0Xr5i2xx_z4QtnxeHFwqIH6KkgK1XhvaP1UwvTxpDobLR_4UcTaPQtmOnNc=w640-h360-no)

I personally like to create a .conf file for each service under jail.d, so I will cd there and take a look around.
You should see a defaults-debian.conf file that has 2 lines to enable ssh banning.

```shell
cd jail.d
ls
cat defaults-debian.conf
```

-----
![slide](https://lh3.googleusercontent.com/zMyQmEFBVuP430W1cbG4YIpvPCpF9rgAY5cKrCiJnoNWw6I0bc8wqUsjOhfZd9OnwvFZXA_3nTHYdZtKbiUix0AaCv-yZBshvMJvARSAMzG7SLqp9GvH1-xB7k6yskTfxf96AcWJOvxF2psry27zgSTD9MSgXz_O7WEBIj8WPOw9n1SAxE-QsCZUmJPhKRf2PF55bk3FM4tO9fKK4gnFbt6FWn6KzF4qJqXx6buqy8s8XFs3lahPdt6Rb7i6YiqDAo1eHcMZUyerGjYGla0uvb55Rzwp4NPKGBrx1ku-DD3FNhM-mWThvfM3GFVC_ljJ7scFrzdshnVInwvTuFOKWakLQyIHsKy_Pan_YVnKSwj43doMlC6_-zLq5xWjPJ8cICX4_KjaraNKwueHi3cX8OaSrU_TWfAzWVmzyM1nvgp4-lXumqlJ8jubU7RAN3MK0Gme6BwQJnhW5J_22VMzHPZP3m0k4VdPKRMYm0nTxzT00iXR7luqTAwBb1y1wgQhUT6UyQy0jwk7xT-yz7zsQ_pmR2sSAkIaizcv0wghVr5mZ8FvT1wENdZoi2SN2pHFNcOGs_r11gVp8gtxZY9all5VBGnxs7p8A09ly7q5d1-u0bNLbbhaXfE-93YibflLQL2bZ22raVeVNa-Ga0H5UB1TvfFnnqk=w640-h360-no)

I like to rename this file to sshd.conf and use it for my ssh configurations.

```shell
sudo mv defaults-debian.conf sshd.conf
```

Open the file for editing with the following command.

```shell
sudo nano sshd.conf
```

Add the following lines:

```text
# whitelist
ignoreip = 192.168.0.0/24
# ban time in seconds (10 hours = 36000 seconds)
bantime = 36000
# ban after 3rd failure
maxretry = 3
```

As the comments will allude, the ignoreip line configures IP addresses and networks that we do not ever want to ban.
We can use specific IP addresses (such as 192.168.0.20) and networks (such as 192.168.0.0/24). We can also provide
several of them, so it could look like "ignoreip = 192.168.0.20 10.0.0.0/8 153.26.75.122" to allow all of those sources.

The bantime line configures how long a ban will remain in place (in seconds) after the requirements for one have been
met. In this example, I set the bantime to 36000 to ban the IP for 10 hours, which is 36000 seconds.

Finally, the maxretry line configures how many shots the person has to authenticate successfully before they are
banned. I set this to 3, so if they cannot authenticate successfully 3 times in a row, they will be banned for the
10 hours I defined earlier.


There are MANY more configuration options you can set up. This is just a few of them to get you started. If you're
looking to set up something specific, you will most likely be able to find something through google or you can ask in
this video's YouTube comments:  
[Raspberry Pi: Install, Configure, and Test fail2ban](https://www.youtube.com/watch?v=FgrKgyBnR4A)

Once you are happy with your configurations, you can save by pressing Ctrl+X, Y, ENTER

-----
![slide](https://lh3.googleusercontent.com/UPRHFL56q7PxeEya8eZ9DR7CNyLFriFmZoFiARC_yULUFcCx0V84kSicqVi8qN5FUu5UkZeihcrNdXzCOeoagn2KQ6uHrnOmzdBE3kVQ1dCF6lLNkahGzHBAeUdJeNSn1NZJh8PIiTTLLffx7RgVC4f9zR9KkMW99INpjUfezonFeo-DaZN5wZs3OJgE3hiywhTVVIrDTRNCEOsEWZ_yj57Twcl9r3eENcSnGs5xjtzfK-gauRB3r1dAXIyuPoQPSbdYftDsvi52b0ZV7F9BLP7DDj9Pjwfz3cUZ_5uKfUnv1R0sDyy62RzhGgQ998jI_TgxJzbfPsjOvXJVwATg7ASjx8lFFpTisWux-zrGsWWRBGaIM5phty2bHaxZUkSt78-drtAm1T2tieagg1Tai5OXr8J54cDYnan02dUWH3vwY8gx3ryfMhQm2xDvPpRiBWjYqwdXVcPq01DNFx-cFRe0_-IbM9Nn4vSTOU9t7s2waY6-Vawa4krmWgfse6t9UKS4t4LOGq0WzExVQJ37IVm2Id88aIv0pcyOwzpPs2Weyc_YOmkHpn7b3vOGC7y3kTeIqruFA11XpZdSbBQ6g6BZK17P56fduHGN32lINMo7dzOJ18hZIX9WaZxbUyPnzFV_XHTUsWLuPP8Ag0jlfizPHpKllcI=w640-h360-no)

After you are finished making your files, you will need to restart fail2ban. It's generally a good idea to also check
the status of a service after you have made changes to make sure it came back up properly.

```shell
sudo systemctl restart fail2ban
sudo systemctl status fail2ban
```

-----
![slide](https://lh3.googleusercontent.com/sxKO_a2v9-43n6KOP0jG84UD3te5ZLv-XPA6QgZPzuBcED3vTGJHzV_n2M2T7RTxmliQ-BxMxf0bmxUHj6LyzRtL-qL8dXoly-RbwzaCASjb2V6AfLQp5Ir7HxoFRqnCxgqNj5OzZcBLQYkFi_vLJ8UbwAyexgmQKo0etwtOqr7rymJ9vgysPL_col7PaFhwLwp1ReSRqp4x1HvGFy36iITPomf47Z2jlBY1I0EABpv9CMCJIEu6jj1ERyV6bWQIYn70YMqPfDXav1XxZPVFkbHSFBoC113F_-a8ofXrZ9hVvStw1tBgw4e4ByPfVcLIZDsLC6pbLg2EzLuCQsy-wo4y-GUx_BItCRB4XFpUTLsy-ZtEu5I3k_DbdaV7DS-v_9l7fvLRjDBQzK1rrR77XieYTmPoj6p5LZRWixWQvFMszIwfzXajIb6gIpD0qrmEg5yVZORXbIwRyPrwpnqwwlnl7J_SxD01SjAC1yx_dwP4BGZYA3lDAgwwvEFlkFzl8T1L8CoaqrWQqS5M0GvCMokUUSdCkvkr15Tzw0X_ycZPN-ozfh3jgdp3nJDZXS-oAJYpTbzDOYjhWFQhLXD0qlmn_JciOdRCvGfPN4dAa4OsRVfPzFYt90ukN1XBpI5QAOe4dA4yqagJ50wxnTdmKXmXFvwRfQM=w640-h360-no)

Once fail2ban has been restarted, we can test it by banning ourselves.

Before you do this, make sure you have some method to access the Pi again. Because my Pi is local, I can change my IP
address, but if it were in a different network, I wouldn't be able to do this. The best option is to have direct access
with a mouse and keyboard.

Ban yourself by failing to authenticate 3 times. Note that you are no longer able to communicate with the Raspberry Pi
in any way.

-----
![slide](https://lh3.googleusercontent.com/D7MX5SQc_5Dzhy9Fh-KHKBr8CDY2HKa2D7bNLhYqsNghiJTqnes9W2SpHn9rhM7iQqJFe17XPLXTpXq7S8zmfFVTki5BSaFfLZdT_Xzi6AkpPHLW2Xsnnnd621VrY6pDn-Q9D8Dh9Ci6QAOfsOgAMJEoCPwwV8jiyGUZhkyV-S3ksB53qBTB-EVcuhnvMlywcqCfA1eX_BXO6Yq1FVbAP3OxGvW2wWvEODgXe6DibX_ADIxhMtmRk95II7-cgihk_V5n4J7qGb8rWc2c3AfR7wSyv087RwbnHtwfCsM9XH6oYxls0wm2i0KMZMJok8j_14d697dh9eDSaAyLyCeG3BZX2aakNpsHNir_s5z8pkNhC0SvqqonwXss-AF7kxiW2UHi4x6c1TBYoChrdBXB7aiDF6nX8AxbfBIglHt2i1ORhAcXO2P5g3rmVzl9-ltW4Wn6nSiaPxfCzySuhQzYagELQ_Xdo8yHqN6OogqpKdjAUdscw0dpxEPmPQplaIGAEhpDiHf9QKEivVho5N3ooNUjo6U0E6tWUmaL_7DhHtIuqN06i0ZFi46wsK0FmV1QoNLgA6WTa8D42NBRwubdvtQ91dpx3Y5TW41b6lMOSF0bqLTrumxZHsICJklPl2Ts3n8bnvW040_ML9yFBOWfLcRCb_9qL_g=w640-h360-no)

Regain access to your Pi by accessing it directly, changing computers, or changing IP addresses.

-----
![slide](https://lh3.googleusercontent.com/3fQgf-hZnoquGPjl81ejdJAR8PKEiQasu85ZMK74bZcTXZVG3sNPqhpgSHNwfNaoRQ_IZAV2AG1tiFlyQPXDW4umewweGlmY8nQ88T2eZfxoRpTNrvwtnFuEHG1sLbRatlGPMnl_pkNQNVc5LuR4i5kSm3JyFQSLIx-UhHM7eUwvSAq9SNmEJ3nez95W8fAIFkScg_XPxBMNwOG29KWu6ehxc8VaNgR_GxoEt4LKYPoqiBon8L7NzxaarvsgVP8GOglwTrJZ8K91SXqxXqXtUH39rxQKnT8W6ruLyJwDqRZcTKCuSHXiDHiSOCquYgqjNhjK95-_L5CCKkm_tGWCKtI2TTN2JQQKkCNHOoHf6cN-oSwhZ0L-mkiXdrk2XWyUaxBqWrce3CX27L5VkZgoqcu6LhpC5odrJBbEDveaczhnwv3w6_4ByS0juiQuqomDscKWjhSRxwuef3tr5pCdgL_7uttasgGitHfO8_zre2iN8PEmqVKnXEemEHOy1LgGvZ4M-7Pqu1ltNYismPEuaeFCFg0FekKf93yoaXPwsrvYP-X9nyYDEZUPcsBtS4jvT14o2yYq0VtUNYJjOg6S5lIY-ipY6sQFFm6J8ui0AeZ_7Li9WrrFENPC9rWkCVf1a2Ogftvf4RrPC8QjcsLkmIrdO17rwOk=w640-h360-no)

We can use fail2ban-client to see the status of the jails. In this case, we only have one configured, so if we run
the following command, we will see our sshd jail has an 1 IP in it.

```shell
sudo fail2ban-client status
```

We can see which IP is in it with the following command.

```shell
sudo fail2ban-client status sshd
```

We should see our IP address in the list.

-----
![slide](https://lh3.googleusercontent.com/UmYoUweP-kovQVMoEUNe5hRgq7Jt3gnSFer3qy6MaL4Cbe4fQveQ23xgzAyafidkZ3fYd2nczZQ6g36fXnlcyQILt00WC1_rohoeFdKYbRFU9IYhrVN-tOivQG-54tnffEqHdD46MHbRO66r0yW2mg4A1NmAcXGAd_T5lQ2xgM05pKCGeNZE8xdDmAqHkMrwaF5jiXbLHjkfRb51mPF2puey9krQ3zzLzPpneSzPFEP5EaGoBHxwTlEZJXmdEqhsk31hA1uMKYkmA5YwUl_pqB4dSowzaNFeqVwWxf7Jf2kWS7yo70-SEDqV7VamAgLMCtnXtuBtyenkwIPNU6EohWxZZNHJWVu14BhUbvbO4hbNLYbt22UVFveCq6uCRrP8vATXPz-yErsOwyPfLsksBLgpKBUjNLPRjAp8RiSyXEuZwjUND_zoGNqGt3ZmqOqaNyV0mT05P7ESuQ9aHI_WUhY4UcUBOTOUCzmGBYjwBQa78Yekl3qSe2vPUInra_1KhJa4OQiixzLvczr0XkaUNzqlP9LlbZd3-MApmI1B-MDhQrMyvFjSwVbuA0xtknRcza0iqDr8gdmvYwWJ-qnjOI2WdtlAxvLQfuZklfnTMahkOieZhf-pkaBHoEyD97znw592vVjBTHbeEiv2R8rqWz3h6TYPMww=w640-h360-no)

Because we have terminal access to our Raspberry Pi, we can also unban ourselves fairly easily. To do so, run the
following command, ensuring you replace <IP> with your actual IP address.

```shell
sudo fail2ban-client set sshd unbanip <IP>
```

We can ensure that our IP address has been successfully unbanned by running the status command once more and ensuring
we are no longer in the list.

```shell
sudo fail2ban-client status sshd
```

----------
VideoID:FgrKgyBnR4A