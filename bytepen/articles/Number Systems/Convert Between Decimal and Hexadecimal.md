![slide](https://lh3.googleusercontent.com/SKrS-GA0bJqitquwAtz2fqeKp8Bj6juP3gYlz8fvtLhXuP3eR5vNYXnmle2a-csElyaUTKov9PVU0tiO6Gv6I8xvuQ4_zPfxBlrZ0koYFSTVOffw-pid_vx1jtYYmFjxbL2LzjC06YIPXOylJwo2cufkMowHhLU7MBvWaeUHEH85anpXxM_kgtG_qYJ5K_S3Szuinkyn99z53SFZFP6iBFGE9ku5wSokHIHvgNbV4sc3Xt_Ah4p2GbwbjyvjRkPrWh-U5mn3ipwCcoYOpMhcj6MAzdlE0-kmd3THYV-UTdz18BGyWfZdHJYRTj5_qGgYNa8SXo0q3EBAL-DSsyyBuefYLdm7Mf3DFkgFGDO4vLP_BzNXvcuaMO0CSpdAgtBSwgZmL-0ncRssDQCccmNCaGiTkxUOtRqH-7Bki53qbLcEBv2eTWC7IVMGCiZ_vmHLf9gIhe3P9wh2BtSFyRVNtBf2tNTrgBkZ0DZ17qXQUqwUfBT1MtmQlH3UssDl6ChAPHpUS7VmxY6M8J-vRBHQugtSQUOKkTYAkrOWn2TRNCSk2xSsSyXFz0UWxCaAIS4G3xaTjYNO73sLbOT3uzLd1eWddsXPJd6ID2onBuVWS80DpxEDPn8U2uZscN0eFTKdCuPxjDUgu-w06JLEYUSBo4Pg8llNhGzGAXv3kvHlgLTJn5dtDgCfzh8CJlQ9evxBuUgLL6VOXeZe2AtFrL00leqq=w640-h360-no)

In this tutorial, we will learn how to convert between decimal and hexadecimal.

-----

![slide](https://lh3.googleusercontent.com/TS7cLUp1GFsB-aMosTE_YUbLlb0Lq-etauEt0jvX6AinXJFyDzSx8adng8lxOkso-vb5s8RD9zzOJ4BBqE_vqb5dpdYzYocYJtNM3h539hyDH66CCG9e58Sb1ga0PcXHVAwOCEWRvoqyAeOxob_3YV8-zydcavdqF0Vq1brFdhbJ13Af95UU0R2Xrw93k9ntu6i8ezTV8RkBWwd_DVqwJG8KbsAegZeEtqA4qdchOvKRrRiSZxS_9cVJP4nHyWKyotJVwQq_WgxorW2rSLPSUY-wS1ElP0m8pF0DKHKIstD7nTEC1MqAS9seKDcQ7V6_-2ZTZ_mm2CuELV8ukeJSQlMP3cdSifc50r6nBxVwT0bxTI3Ex5JzTqT39pJaoYGx6-ZtvgKfAiCf01zaEye4zPydWFLXyFAmcfWoxtAB8QrHHIPuTLGugrnlezPs8toA-zRaeEsDZxHwIjAlI85JUCxQw8ruhSwitAQzWZGHxNcQT1D78JnspUFekUPm-U65XDsDTlNQZ0pmrrH7cn2LwQJCCBhttUv29_BxQpYw2DeUmIXqDgFjBZGztAC2FVG_yrQPw6SSFbHMA3Q0jv2hcgLMlKaSlSPnZJsegjqEkwg9vQZFNBwXAiLB26F_isbDik5dbFxJUeiqxjOX4gqg6b5v-AKkNBk=w640-h360-no)

This one can be a little intimidating, but it isn't that hard once you get the hang of it.

You should know your hexadecimal values, a few powers of 16, and have the ability to do long
division, multiplication, and addition.

-----

![slide](https://lh3.googleusercontent.com/71ZtFpXB1LdMAF0h8dgX1bZt-kf6zHg0Fv01FFYDgvOsShSYYoYQTQ5xv5t8vgBNj83gK0RkGG_rTtcc5r61OtiWvIS-_xruKiUWEXkW36-q5zbs2-YiiAWV9hvH0EWU8Vbq7WoqGEnzTYrZe9SnJUke4Puv6sRAY7kpQ3fLAnBKhl5L9K_DPThjho4pIkJUG9jv9SuBhWNQ50XpWen4zlEoiWhrSGDqhoYBpiADkPm3hqQhgNDdL29pDdXiyhoEhj5yz8c_CMhpjSCkxIYVLvC8yW-s51vlxMAXsZWIkbh1E3aSeKa0Yu-vYpb2Ial1x-06KyOWJb3R2msNKPLcyucbkse7aY14QAS6Gy_BiAE4fr9vH7zm6q7V1Fjf2NR9FNvgGMKKO5GxKzy2HtGC5cXlRv9rxGcMElrv3NnS3VWbDjm6ULxQUghY_B0C6rJb-KbifA_Ny4c_gt0Ot6IRPwGqk-kUmbnXT6o15CywfQiBUr_T2LySuZRD0HL2Dvk_XjDEbK0b4LvHSmeT4I0C7MzZPAFfXblX1HWdaUmVUFF08CnlmQf99dZDy2rJUh1FUGMmVDHExhJD0QkYBIa9pFRmeVZazA5nasMwNrdMpffHs9OLzceVveAkNG-0EbpQhdVwH2YPwvh4t24xVvhMfsXuyPB4Y8J89z3ikzKjbi-Z_U8pybsw7pUYzOb_E6S5ufLfaeka-DpA96qkudkUVGE3=w640-h360-no)

To convert decimal to hexadecimal, you will divide the number by 16 and note the number of
times it divided evenly and the remainder. We will use the remainder as our hexadecimal value and
we will continue to divide the even divisions by 16 until we reach 0 even divisions.

Once you have a list of remainders, convert them to their hexadecimal values and write them from
right to left, starting with the remainder from your very first division. This is your hexadecimal
value.

For 53,176, the math is as follows:

53176 / 16 = 3233 (r: **8**, hex: **8**)  
3233 / 16 = 207 (r: **11**, hex: **B**)  
207 / 16 = 12 (r: **15**, hex: **F**)  
12 / 16 = 0 (r: **12**, hex: **C**)  

Taking these remainders as their binary value gives us:  
53,176 === **CFB6**

-----

![slide](https://lh3.googleusercontent.com/KkHrdcmkyPsv3B-4EN2KWCkXFSHPBNF30B1cNea1MoH6psnurAUsIhIeeIAw1d6EGvcolsvZltWIii5PaOy7c2jF_Qpb32WbU3Vd0fj-eAx2J2U9ORUb2wSykSWK21DbD8E2IC99mZ5nHLNSDng5J4Dx0r8OOpW1YOclqcAuiNtJbnw7c2nrRNGyoCPi4zc3L5qSIA2juodPJtUlqhlet8vKclB7XydKAUM2PgAMFixim3X-FVNLfcmuiFaVYLc6m0sx3raOPNzOH87RIR7d1Co0GAE7GDYCTHeCnkNudLdJJTIxanqOES9ftOK-tGQEmktLdCB5fc1kZSSew5XT-Re3LFv78__bNFuYoQ4EBRCcvwsmHeAmzafgvEo5PCbWiphNFlciHH48GMFfIVF-wQw76CiPg31WKShH7fLbPTw7lCzjteGpWj20WNXxugLn43kr2ZueFAPW8bmeQtFI5MIHm-HF-8OKvQiqW71T4yMhmdZcFXLedvWCmmOBMjgVuUXQ3QKwPt0k6iuAAkQzMBx8R9-Xu6dxcF3r5lrdi2Ye5_T4LP5DlFwColmb0hXVEacrbFrxPcYL23iwqEv7Ev9jlpr50EGSHG-gwcK2UIrWoLrRX6Hs13ZbVT4IWRRq6oVxCzSaLr_VYm9GRgyeuaXa6iLS2cWBw-l4K_jiCkbB5HyXJL8Is03ui7tF_71ezN5YMYeiFQ1E69AB2O4M3pz7=w640-h360-no)

Converting from hexadecimal to decimal is pretty easy until you start getting really long
hexadecimal numbers as it can be a bit difficult to get higher powers of 16. Start out by writing
your powers of 16 from right to left for as many characters as you have.

Write the decimal value for each hexadecimal character below those numbers and multiply the
decimal value by the power of 16 above it.

Add the products of each multiplication together and you have your decimal number.

For A4D3, the math is as follows:

11 * 4096 = **40960**  
4 * 256 = **1024**  
D * 16 = **208**  
e * 1 = **3**  
40960 + 1024 + 208 + 3 = 42195

A4D3 === **42,195**

-----

![slide](https://lh3.googleusercontent.com/zJxHZQQMzsPPBqxdfW6YtUPeNc2lfEcfPrRN_QobwgXf6SiR8kFUm4vzXJkdZs9yDeBKwxMKlJoiWJ-Iv34VhY_of4DHXmhYDyCkQSb6hMRwTPo_AedQB7B-GktWjiEX0z9AfiphEOXzb7mkOxv8pTcUynrhMg8XcfJ36j9EBwNgqjlPXpMJIzjGFLwDsdoGPqSTxFNrF2JNNUFa0hUqqj4ah0gZDYkr0KB5i8T_nz8gUTN4EfHQq18TXGfjlUYK3U0F9QO4ysvXp8XIzuPtfN2fXDOxaUgEemt__A7uBtRWfwby4XsBkmgI-TzwSC8HfNSO_5r8ZRRpJtmLp2EyUWfBDPR4fEQesb8m-eLBjfJLLtaCZLZ5PfFYaq3zji3wgK0hSdIkn0Mz8UbYUt75W_NasCpxxK9G22HJUg8XDil_wacCmme5ZbnRKgg9Y8o9XFjYWlU5PVZkUJwIlBsPYev41h9aRZz7QeJkD0IZH2cPDkFN0GWpkGlj8pwff0IxosQxd9qo1YR027QhrFlsrgeqdGoE4GIa8jhHZ9ZEQtlzH25vK07fCMt0NmzWL4JLp5xf7A2shqQ1J0iAiYlcoOUW86m0rrK3_zyh6uxFWaGSUeNAisblXFQ9yanRxMLR4cfxdXndnAvrmD9QV8rRdPVhOSwCBYXNfdge_WhAs2V3jaSwI8pvu4ZUq1Qvo7dBxekzR-Rpzq5m5iMIsRny2inf=w640-h360-no)

To really hone your skills, you need to practice! Try converting these numbers from one system to
the other, then back. As long as you start and end with the same number, you are correct.
Otherwise, there was a mistake somewhere.

----------
VideoID:wp5g35fpFJY