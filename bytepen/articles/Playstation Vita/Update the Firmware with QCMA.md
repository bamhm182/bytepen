![slide](https://lh3.googleusercontent.com/21nMubcRK90P0BmmWZHEBg1xI5oXJ6hiWWKE-GirAavVTavsQhrcH4_txEonxJo2Qo8EE34xVbon-odepT85Q3axED1PP0CgcanSOYAbOySuO7aHcHMkMMz2xQO8qIqBCmvLSJAQ-M-bKrrYMznuukZiHL1PPDk3TyrvAZEO8z1DMCoeBnXMh6v-eX71EJ2nc8WKBdhviQyZdt51KgdhUbS2sZ8wKQx3r3aIjCy9uAuPI_H2Bwbtb-xGfj6XW4zodWdGLoZ2jxgCcw1mONcRS7iGFfvUWFPcVPpaAB8oFEI78qKpN8Ykc-Aoi6w3fnLg0r8dkenPawA0IzIdvXxitp91wKUOp30h0YYPqz92-I7bhgiJgZIwP9EYdaJeRK9_iVR-OR67cCwD1EkWozVVlsJt559FnaDWW6XLvLQAWM29IloT_ZqLTaxhwkWAZmNEvmQk7vXNkxWT4q41f_37RW6f64kYeYfesZOOraDR7zYSz0SZdG0OonTnBMBrJrEzdBgdml-F48QzuHyFtQirTxa4mWqL5cuCfzrXF3pLMcv9PHHIAJt7VxQtStp9erbuHjEsKMuCPu-VlVVm9t_111Xfj8n-utiJOWTKKJZUSw01SdfcWT4Lfq-cxB5xBpbm74mh0im2tSxKqYKzB1x7KAMvBsAMGwmFraj5QhSCgixKRqqhiV2xj4rRv3akThd6LNJ7_lTFFWcy5f_eLf3KiDak=w640-h360-no)

In this article, we will cover upgrading the Playstation Vita Firmware with QCMA. I will be
upgrading to firmware 3.65, which I believe is the best firmware for hacking, but you can use this
for any firmware you choose.

-----

![slide](https://lh3.googleusercontent.com/2v2481Cp2s0dI7Gsec4rHl4xLxiiJsXLgyKMqJ3P5-eAy-QpFWTfBzxfeNxbrGrBnU1hQjFpBjzSYvnjhw5wjZj3-gzPyvkEK5aHSSupI7OZmPiB8gKjNSwPspER_H-oP9P8_jB29k4gOQGWUYi4f0r930Ioa1F_MnK1cuNxGRGmu-tDHWD4wzOtx85bmM2G8Woz_tPb2kszwquvWyeWN_mUN94aYH3uxUAEYPsI58Qe39todyF33NGRXhJriG-TYVOhGzVt5YgdoVPMavE5hCKgn9Oj8WMWgAjhluuw3uVcheGu06YSsVFhoPzIzy2Wo_k1FBkx9vyuFyupLsqsHNfin0_1Op6PFnqJcOABirQVPiM4rvk0rhd6ISieDVmpK-L-lmZsureF8u-RGUcZNKxiuURjfSI8vMsBqWZ7MONP8maLknerD7rap1GnfdJhd9dvoSW3uyDsWyB-v5cFs7YRHc-GfoDaYtF7-D4CZQ7ySbkQYEO9WzPdhp5n_6BE7AVNCGkYF6QWs6zX7nAtcMq0Mn0T5dQeb-VN2hfqjGzl81E-2A7WWBKVrmkYG9wgabTzvMJVHmANsV-T1NMukxqWPKCxu91o3T1q4rPAYs18Kw6SbNOpdeNXCMbb3R30L5h5PA9ZYligtI3zSJMqc422uGFSJ31f4sXht6KFXyfZADRkK4OO3l2DQ-aPaJxazwAFqmFExrnmiXWZA078Wwkm=w640-h360-no)

For this, you will need a non-hacked Playstation Vita, the
[QCMA](https://codestation.github.io/qcma/) software, and the .PUP Firwmare file that you intend
to update to.

Firwmares:  
[3.60](https://drivers.softpedia.com/get/FIRMWARE/Sony/Sony-PlayStation-Vita-PlayStation-TV-Firmware-360.shtml)  
[3.65](https://drivers.softpedia.com/get/gaming-consoles/Sony/Sony-PlayStation-Vita-PlayStation-TV-Firmware-3-65.shtml)  

**Note**  
If you still have Sony's QCMA software installed, PLEASE uninstall it right now. QCMA does
everything CMA does and so much more. Leaving it installed will cause issues with this method.

-----

![slide](https://lh3.googleusercontent.com/EbL1-CSxH3c3RviroONVc5pW2WUf6j7j3s0H8nabQM6CMG23fIe_sxlXLn-EDHLNNoXiE9rWq7z8blhTJZjgccaqbOSzSjCRkAMjPbruPNdBsMZJ6sRbgRDH45pNgIqrqO80HK1AjzDdL1rzbVXPsYBcImS5OyMs17YL2bwb-uz8Xj5U3vcqdefSF1QXlP6H4ruTZVfVtqcEHjPL3TSJ0U-jJtKwfxCQuZXGZnYRYno2iVPtn_qe3IIsUQzmERO7WP8PRTPBp8E9yI3-Xv-3KJxWBG1mOLS4YEBA6dScubzYcJFwWKJUh-v7DxlmfrT43j_8XfH_-4hqBNAjX_JerhqiKOJO5YPWhCMnFIAzYdwt_buZcgKpG_rWZIKB-y6tli7Ej3H7RIyEsJdCqk1GFsAFNnaVPzNTGJsGH__WnUwbtdIEpFt0oVOZQ5BtSFe6zEcjgJnXc4VTN0IElPLuHu39SZWANBUQzgVqk-rE_IeC6QD2laqjYnXypV4G09f-crBvvm-Z6QMWt_SgDcdKVZF_KJfekf9aVrEWhEVt9tOucn0l4mK4AkfVrufhS_TWLzcbDOblNC-ukJa19UlleOdQlvN2548NtEbPNOYWajbmcDobr6dgtCLbwiAZNm0dRMfIN-xcvMX_cK1G2dW-1SUtp_2QRsaCjK1KruE9skZkFaObY6p1VY65KKTmdYP2lYcqIGz86yBD-dxQXDVBSF3g=w640-h360-no)

I'm leaving this slide here simply to say that you do not need to worry about it anymore if you see
it in the video. There used to be an issue with older versions of the Enso installer that has been
resolved. Make sure you're using the latest version of the Enso installer and you'll have no
problems.

-----

![slide](https://lh3.googleusercontent.com/NO2Tk-maFsm-QFyjXgpUcKjzEDwKgQLcrR-riuGBGa3W8hFy3GpNzQQ7jwb_e2m1-uJ3rlPA4F5b32jEOIxJiatXHovYm2sjEhUXO4h6HHObuDJ-yZHhCVYwWCqHQ3OlLFGvvfdVI4JdE4Oqgh8yPtSKnO3HwH7-80sqjnlNKEgAM_ANb4YNGN1YUynMOHLuuEEwtI4TmJz7TcbaHYWGrKg7ybZF5jZJaiMGvAuvtvglwC9yPdc58ER_0bZFjGWtRqBCuI5QjVjqtpzZ7zripJLhypZjeq5tFNhRPinkzBaZM4UPJ4SWOc4VsZB71vC0hWM17S-wvcLBzDB_cpXdOI1isW723QSPnRdY2Y64zhLUfy7EkScsPE_NsqrJBNhYAN3Em78e375RX54aGzB9g5Q6PVfhScHCW8OYLVwVWhbxX53q-t_7qoKeIwv4_lg1Z4a1phA4BCjRdEVhNxSH-1-FxnINQb0cP2W5DxgauVbv4i89FLPri92Z_QaCZRGWwPs_V3ie51Q1TJ2Wq-h6i-3p6F-ewDd--4NSU4W8-An4J1c21D22ymadCNuh30hQnuCQbtWRhikOpptfEHGRQie_kU1vovYEUGeNIAGQ0bJXXTSSh9ahQ5UqIzGx1jzmEVzJjjA-eLETBJkBqPyhIrboISyxeWaoYyims6yUHHUkXhplKQlYHY9QSKdwFPpMUGpXvafQgHRd_Y8qIbkk5VnV=w640-h360-no)

Check your current firmware by going t **Settings**, then **System**, then **System Information**
you can only use this method to install a version that is newer than your currently installed
version. If you would like to downgrade, check out my video on
[Downgrading with Modoru](https://acceptance.bytepen.com/hack-the-playstation-vita?v=downgrade-with-modoru).

-----

![slide](https://lh3.googleusercontent.com/8BAo03UD8hL5lhMPoLJN1CfnVJSEZ53QWZ0h54Le9TMnj_wYVWv94PXigR_JCAOUmHDpF5dKCTlBzrjOg6DK6BjnJ6bfwjRNo90Rp4w1PQ63s3n43AbzVAgCmWYvnixjv9u4bE0-YCIYQ0sMef9IhyYk08hY-AcAGYx_AE5TGwnIdZGmqehfDg0d14L3OLfx0NfTcCCgxZUuKItXhHgrxMs6fnFIroNAPX6wndupZJAGT6r6l7v2vTCBPK3NM4AoHMu4ffXSTuKx96eQJEyUpYnsSV7cug52oBUIQCv-pGlV_illQGE-jP1tJB9zlW_drHoMub9AijQqvp2V9fz-EyghlFqYjAc7MeYrLFZn5DOMx0r9o4jIaoSf80fF2yYijYEMBgDbmTfGK8hDnmZooGaBeLu8iWAoy1dGDAhuS36S261jN6hLvalJCZ1U3cmXtUXk6Q7slFmJJJceQDzbCNUSgUtfDUWQ_wB6Wn6o_oKYd6c1Gonsa456uVH2iDY0tXuhlbDQjUDpxaYaJodJHuF_N9E5N9cL3Ii0BqPXzPzCHIJHqvGtLPYfiJfbRpXqO5SLQfNEt0HZPc6C8XXSy3jAirleAdXqLfXJV-UPbDDX8wyaMPQvbZ7BCXipg2J71EBrbNkumD4vDO4WAtv_VjLJ-UtPgbivXUsqxq1ZopoG__MLBV7MmJm5fx-_3S9S5LNO4aQ9ug7BFVgQR1z_XRG5=w640-h360-no)

Disable Auto-Updates by going to **Settings**, then **System**, then **Auto-Start Settings** and
unchecking the **Download Update File for System Software** checkbox.

-----

![slide](https://lh3.googleusercontent.com/OPHyZwUx3WiZRLtosdhYdG-HRfsB7sEuEzvcS_Nv0CqsbUqt2Mnq2t4BOvasF4HzD9j_RUJZIwg97yv4NNJePr5WxIMbdkq9bGEoY02C5VvkwBkXQpvBdPl5QHE4DacPXSrjrNC7HS5M79F49kCBPblh6vQMap0MUARcdqyTQlgqrJSzpB86JIb284ip2KQdX1j8s87w1dC6_GbrbkQFDReLwdtDZCbNvLdO0nsuqdzC39ec0FyNnv_7cTmxWOxr9dSlbBX5HGrpSXD8NIev6EMavzcN3WsMHMbxgaapym-oqiFjLgz6_kDYjqIvezZs7u8QzVe-9GQtzuTpwUo4PMbXEuGkr88C8_9Oy1a2jgP2kLW1tqJVzaC6SEQCEyJEEpxYCf5279_IGt3bd4iXCUwbliAj5gsW9mPJB1cNVnufrtRz_HDUOul6x1wQNIk5OUzRV9rP88DiFNcO1BtUJy0uuxQ9yWYp5ruUIbTv6usYFYsaU_QyACyCFR3xv26xgPmGL0p157NJW7wsyPa-J3rlB_kYfp-qAzHtJ0dySE4u81EXFOvfQrYkFsG3TbnwxMojOCSGQ6_Ub8Fvqyetq0rPJ0gsL3_khat-9T6acoo5MSBU9PW_KQrFQ2fwseHjZpphHVS4Ch9b2emweLCjJY1wHXDJJb890XWWf9BV26F4hWVH06EakXLhk_8UvBj-fUvCIt13ik2-HpEFx9rPDDat=w640-h360-no)

Ensure that **Flight Mode** is enabled. It seems like a simple thing, but it screws up a lot of
people.

-----

![slide](https://lh3.googleusercontent.com/FYUSrfHyHi-S9TqsFux7F0BgeXLj5M31EUAHdD41p6YwYCVt_3zAJPU8f3i8dp1We7DUsWTq5MzLz2u8WiVHqRYYOuL4j6WuMLutYdjrvrOFByCWu0R20MnwumO3ty3mEG4lYCKdRxf99oflIIdWmjwL2hbu_p5TTsawsKuBvVKXOuDYLXQRQXRxI2nAd2L26-HsWEXKHoqxT0T_jSZ4pPqKJig-8DciesHpqOjRlhqgeZmLhEJMgCHJjdpQt36PQt-5cr5iCFr2PLYSZGfZNR6V4W5e2upQuyDXoQ6XaS58r3DOsGQytKNisXyffOcy0k4VIwx71NN7vAgdxOhv_srYMnVXcjFY8HvomQQbH9VAXPPEGSRos1vwk9-j9AGSpnmKIH7eYoREFRxOQSB3QZNKpEPm1jfkbVl5Rn_jjlQcN3IbUW1IupCB-BJXzpPdmB47dsI6o078Kk4TxYnX5r286uyl409ADlY_yDkcdbipIgDsbXAqhJqCmtMMSMgTbcIUcCTwRnqTC2hPdcnoQyMZzImYt0r3K0RKKSn5al9MBV6h4X65vS2w9Qm3WLfnfWm6fOLwGwG9lLOMdSoUZ9bYQyFNstYnL8MZmGtTZVayPnvFepOzNxsoqcdB9S5lgULTasy5w6PKs2YyENLD2n8IbNel6e2K8DR4JZosPHYGfE4gl8CLvnMz7MCABASl3FNZfaoRvRnctxumT88fR6jh=w640-h360-no)

Open QCMA and figure out where it is looking for **Update / Web Content** files.

-----

![slide](https://lh3.googleusercontent.com/uBlVi0QRKyz-mladLMf4T5GLvz0QaSCoMbxVQZPPMwcmZ5M4jp3FmtFILI6L-tdOfi4jFxgEKtHJa1AmVgHMSv3t6zGc6kUaR-hfz3eQD0DhQ5iN2rUv8uNlC4vch8v7YhQuc3QXJwBKVw4CuVtp65f_jxnGwj6t4LD4W5YS7OO5ZHIKeyPYmlAW7iXPwGVm7Cv08CV6njmcyFAms2IEL2c-eNE8tKYMmoY2aDqgLJmV0w-cONWxOs7plVNODbnZzgEjIZlIUDSBPs_eTsRJGPWde-ONJBmJOkT_aLrTM_zaeU3IrOI4PP8T-aTVZiA2Xd95Nk_f4Emb8cQzrbETvNHjc2_Opfl83e_kOcy57Ckj6b852qYyWnev0KbiQtN28dzJGrcQS3aWWdAPq21pcIzqhKJIHb3Q11Od5e-rQG1vWBWurM6-GkuhyGNw6yMZknKaf660ReKBViSLNPzmYnffy1_fhqm31Uz4VYX8yJVrgW2DwPC8pIp7VBRszJfEzSSrZoQkTwN24pxwjgaB9hGRoQYbNYvZqmzqiXN6zvvgw4hmo85GrPDKn8hmTINaEh4p7yz3aCvkc3rMyNv8paVsUaGHa4OuSff_6opobB1MuRZVioIVivrLAQwupY_rsG_d8c1jr3ugA28NgNLkcvwQPIz_rrK2X3g7mashIWeyLbbrC3qFQmMMPo7WQ3tQAe8_HSZl4WbeDuPsh1-T1fEL=w640-h360-no)

Open the **Update / Web Content** folder and copy the .PUP that you downloaded into this folder.

Rename the .PUP to **PSP2UPDAT.PUP**

-----

![slide](https://lh3.googleusercontent.com/LI72HNtIkCRzrfD0fxLWQJ4t8spSBXLGoBQD6efxC9q4pZEETXHA7oU4NkHcMkS5L8GZiSqHdrdLgou5eXtEAlNulCskcuYubui-GqhuyfbPYKT7xIxMSs-RlDmjQXmV2iv23fAsiQnsgkaYBIoNPPONLKM3bM5aXB5FVfqgrqH_9uMofmNufK5Ll67CRAYwtnI9esBfx_HMzNO-xBZSs_GX-_oRyhnxhSinZAOyYoZm95OK_p1S4T41rjdwXzmYKwQDFXAe_3o1SSkVI0N_ldPtPZTGn6fk0h9kMcHOjfyyJuN51t2N9Wd6HYbWP6W9IbLBp8XfzXy7rKnBsMLYap6QX90Gp7-268UaklX0lU1kORQkacE4_hEA69MU_QAWRMlyXWJuUofadsy9CNLySbGDY6MwFGzHXMlD4Zw0pU8Uqt49kKtfjnF3qyOO3K5sktdsjQo3XGHNEZ4YfyrL7gbhyTpbcA0Bjw5kwS6hys9iEwNEG0Bj9ZHACDP_jdWEsSD5CaF3OMIKtlWuTUm_M52u04BODS4RShRp9hZl7LQB96QmwOy4AT1MrH6hrCFJSUhfq55SAvkgtp2PSWrf9Y3bt2ehF_X_hcN1vty9IpG8qMAAikrGouBCO50WRV0bA0jA39KZWPXw02QvGRQn6S-eDZHSLVTTGGSqPr3t7iZf8o3sgTmKB9mcGNfEf98TjeazOVjUpipTUqKGFbP8gQpO=w640-h360-no)

Back in **QCMA**, click on the **Other** tab and set the following settings:

Use this version for updates: **Custom**
Custom PS Vita Version: **03.650.000**

-----

![slide](https://lh3.googleusercontent.com/m3tMJ09jkJ8cQGinJJwKaqi-T5rdqb0GvvXBrtntwqoxPp5JEIDfNm6C1NwW0kBU2SsDZM3p5fxgPmn-AqYY-xS-JqqgeMvsBiO19jfSZ3XG5NucA-Ul51IvbpI0jN0MQhBNrXs2CUc4nZ65JJafVtQ-w684KoGKOEBfPV724xUzmCtdsgSlqN_1TLdZzLYHQPrSLaGQUHGWyxI7DGE0048lkyzA4Ai2PBzIfTGVmbop5Ni5eCbtqaLh1x9XYd0Nw_XRHuXIb0_mFpMBLpgjLmFSX9jff6jsGDXEuJwXuZ94zOy_hLgsBezSwGoIXuCwnzi5dmAU4kfi3aJpYq5S6pGNWGnfC3bxHCP_-7Funib0pguAYw6ifTlyP43ZpMG4v28i1I7EbHRrolC7LnqtjbOByb87xvygCVRQ9jEWBbeZoOBF3sx1N34TPUq1ZMJ2aX1GnwkgZCtTJu6PPM95o79PBfmnjFitZNjhlWreJrKzaLvme82rEO463KijQUsSjVi8voFQn_vYU0cK8SCwetu8VmAoW01jyYF_k0_aa0eWJdmKRqzP1LQH-r1kar1gDKRK-_nJKnJnphv7_qwv841LHTPhFhMCyzXczkX0dl0ZC7f222CbRPtmA-ue-AQgWThZMJCqEobRingq5e9a069Hc62a-FIF-FDtmEi5fEtJ7omFAFFUfj7Y_9n-NRe5_OKayIpsOaval835joU0xMSI=w640-h360-no)

Update the Vita by going to **Settings**, then **System Update**, then **Update by Connecting to
PC**. After the update and reboot, you should be on the firmware you desired.

----------
VideoID:QbF3zUcDnHY