![slide](https://lh3.googleusercontent.com/KC5fM1vmj56rgjZYq7Ru_CjI6jBGCIfd-mKvroKtucjCHTfK6uCopGOGkb72kMpxkec_AQiJdabzROyntOimPg6OUOibrcHh95QOMnzj5YlXVGKhQSe1YucIjJYWHYkH6AIgrtwFb3KZ54ZIzWoNLmTdJ5x-2YhUBgySZLsfwImxLq8U_kbyhhZO9BKEo4JKG64ut-JLJy8au8LOcYFRFXK87rP1oU4eKuXuntruLBfFh0RCanVgsuAy-t9axGelr7mPp3DHTO1pe4wZccbD5UAQzTVLYeUP43KbfB89llezFzbimlH1wYZMMRPD_Ul6dXubCc1-Cx1LwYcONX4Ef0raIEdfv7MyJdsk4bdA_ejmviIqrB1MOqZMlAE7jinoOzbKOKoQp3xfnMHBHVjXL8KDfMcJLAw2ILCr0L2LNmiBqwXmhvua0lEMlZoTt_pQv1DIWT_GOA59EK5rI9XjHmxa2gu6lYPR63RH2Q3cyGOqZxhScF-qBHIQHtvtrXZmFWJaesIwi2UgRgcLm7cDd9HXGGnEW0wKM_3ItKcscPhA4grrwuA8zydS5kzoFwIvglAJh_cFyYhkzoC0gZuiirXjleiJA_XPzNrzrHrm9hVp5SWjODy-0Nj7q5wbZIA5905V0Wku65mXp37t43DWBfM9TdQIhNghNkzf09rEOOJ0J2DuuM86Rera3E1BG8uEdXugEf2f4je9WgMcBQGzBMJP=w640-h360-no)

This article will cover hacking the Playstation Vita on firmware 3.69 or 3.70 with
[FinTrinity](https://github.com/bamhm182/FinTrinity) by [me](https://github.com/bamhm182).


FinTrinity is just a simple script that automates the work needed to use
[Trinity](https://github.com/TheOfficialFloW/Trinity) by 
[TheOfficialFloW](https://github.com/TheOfficialFloW). He gets 100% of the credit for the
Trinity hack that I had absolutely nothing to do with.

-----

![slide](https://lh3.googleusercontent.com/w4OjhtX0Hl7KOYOgIRHly6wuBFcbEbj3nD7YeUUoqCLIeXBtav8iLgRCJTpfAwCIYvojJPVQ0GYIeYSGiLtyR_OI-tPaQCOOOxEOJdEfj6CgDZmB_jBHZlcjyEq69Zup05Pfib2QJUxwRNnnUznb2cBKdFn7hjmAwaQRGvWzWEtUMjPYV15jKiJQC3zTDDKS40YsAqwdDf1d9iIh30QBvkUmDuRX5WPS4ZpfvOLi5TyLevDLBxKJeaPLOoEk2oqLTKAcuWawaZX8NeVpdavIC5vbazXX9Oo7yELyLO2W09WVX4RlX6biOnGo8uyf9rMxhSCwYkoE9wOJjruTXcSHsJBsQrBhXsZUEmfR-WFEWlGa5InWRqMJdh0V-MELHLtdMEW3gYbYh3T3D6X8mL9mV3ZN-fPUiVlsLeMuFXkIeqwNPh5Wi1SetVazVGBD0-8IuTkf8x5E8fElMfz4XGypaO_Lv3gkEs3-XvZveHWgq2sHa78fu_3wybYWN3pfWebkyIh7UxK8xLefKwM8VlnkbGqbcObnOqzuORBGxEIvZHYbFPU7f3gBV7riUHt76hSwCheiwPwsDZfMSbQcDtl-fQAJJxVM_GrLkvIEnYt79auArvGOeZwwGNXvwvBwK7Sygk4wB08oAUcpV9u_qF-fUN696PvSIos-yg1WzTznbH8z-gm2oiAQ6PowA1X0Ihje9mUmYIL_M2nYEO1dRoIyfBt0=w640-h360-no)

For this tutorial, you will need the following:
* A Vita on Firmware 3.69 or 3.70
* A PSP or mini Game (or Demo) (PSN Account Required)
* Memory Card (phat Vita only)
* [QCMA](https://codestation.github.io/qcma/) (Uninstall Sony's CMA)
* [FinTrinity](https://github.com/bamhm182/FinTrinity) (Download the repository as a zip)

For now, you will also need to have the following installed.
* [Python 3.7.3](https://www.python.org/downloads/)
* Windows

I am planning on seeing what I can do to bring this to macOS and Linux and remove the need to have Python installed
on your machine.

-----

![slide](https://lh3.googleusercontent.com/EfPLPrOh4S3ELIaXh6gqXdhP91kLJLPqw8VeD3tcgz72PHrAv2kJCswUvr3pBxukcdTG8gt3OqZSUN9ALwp9FBbxk_eAp-wVyCMcEyDOFJLLNkkqc31Qw4KLrCVB1s3IJ73Kxhls-LEHkLPEOwxEHPXO2A1knLFDu2RwGoNHEfD1V3uFkDhxZtyFhj4XD1aQC3R9q5kyx3u_Ev79X3QxKeNXNdoFHsilFGK4gCJIvA7PagsVcnFxfzFh7xpRn1SQ5W5QIoYEZhGb6yOKQuxK29uqrXn9r71pLczk5PNDMvjwdh5JMJpIuBB0RXBo_Hen3AEg2EUCS9-VtCM5z9VyzxzvDgBLaXWOPizXkEa9zkaE8TsjeZLQZJWJMFbtLlLZlaGRn9E-6mGvUh2AOBxX0xxNKgdlVc9M_ewwaT3b3bR8El57ba8PZBKWAsb7eeVG-IftDdHkRt_CMwoVCkqGRmLu_DB2ml-EofhRb__9UwVU8NuHPa8WRww-ZZLbVsauir-sTseiDVbSLP8iQn-OmKkJLcRUuPoCwgorkM8Wsv2HVEI34vhdgpEHuK_zxosz-aUfcqeoWaXnwH-ITU7xIP8j_guksi6cgoZsQ7T81KdDqPNYBFKYnEmHa1aS_lpY075hqkqT1YFrPvufLmAsUijy1d3OTUIav67CUmu-d5vy4yrPx7Z9mPYYwGDCykSljC2v6JlkUAXzi331Gt8HJ-Cm=w640-h360-no)

FinTrinity is a python script I wrote that takes the
[more complicated task of installing Trinity](https://www.youtube.com/watch?v=OH9ZkMqJ6MM) and turns it into the single
click of a button. I have seen a lot of confusion around how to install Trinity, so I figured I could create a tool to
make it a lot easier!

-----

![slide](https://lh3.googleusercontent.com/P_KqBpTx-Hcl5DY0Ju7QpZFPMv2Y0bPYh3EDxbJvP7O1JXlZJCjSAi3iPPCtukk81zC55LfvZzKkFBpGHksrftvOmrAsihIbyxfKkmr6dpX8H5oGQWpCpK73FDiZuY0xrEwhw1MiZoYb7hBk0uoDNDPVUij36sJlLzTC59ZVcWPV2VfDGwRY47Avs_ECr_-I5j6zzDjcxSsMEtS2kZevE7pK0DudSNbWO4Hhk1RPHceuQ253_YsolwgoPZK8WJ_mjZE7BIMcgKFuDM6F323-l4Q58wU44bXVi7DqxH3Hr9L91A-Lij3LyVvzmmcmEXiKfDFyJhTmtSS_DPptvq2FaPcwz7aS-HVMUo8g5Bn-f3esWFuwqFa31VpuMzJYFNIl3hyS8X-KwVP270xJR-N3Al1zDKHDcUgE4n3WZCYms15__0OMVlqx2mgl1aqdD_0O2yG4apbmuN4CgS0iq7ria5veppwLLjI9FPzAVfP-K7Mr-SE1eBZagYZyl6jTSWwlBBdj-vgtQm_Ok-4cl1YF45wjp_EpVkyTOjNl59dpbnOflNLk-hMA-g7aaVs1BG0EL-4gjFrm0b9NM36cmcNFT1cZhfo9fwaXsKKip5wzAXTgy4lVXweeX19thbJgIvyYZd_d4Wthom9hQbsBFCxiilRRX_0u5OCa5YTNCk3rc71vesa-9Bxx6IliWC1XLbsoB4uMgAvL6KDwRj3DfdhpUIqd=w640-h360-no)

If you are on firmware 3.69, you can just update to 3.70 and you'll be less likely to
run into issues. This is what I recommend.  

Alternatively, if you insist on staying on Firmware 3.69, set your DNS Address to 212.47.229.76.

-----

![slide](https://lh3.googleusercontent.com/uSWO5O6FLAdfNe-wlo2ojK4c2tZ0E-WQ7U-H0NcK4Urg6py5jGcZ9l0INLs2uJ8456sacvzAAobJiJRKu1tIhWlOv0DhdMAjI96PNTolBdbtoMwOrcrokrU4z81y8s67OXs14XuKT6xzMvEwHV_dIFxWhh-9viMOIGmsgAdhJ3qvZir0bYECuUz2RYe3sazqj_ldEqkOb2JDSc4eeRTQ9XqduvyonQDyr_F8w89Y7fyIy2EzZyjJwHQ5lWmmp4MoXgx4F8wEXQ7qSK_28RFQFLZLZZuL7a_64efk8lSmv1iTODIaH3TlBHJFAY41Jdy2cxgnGWt12fmmMcMI1hs0ELouLRQ9hzZmoHB3dT2kh6m6atEGiocnhrOxLR2xYRo7AAeSkSNwqMHjKkpuWtLlOZd5TRCwY9YtjSkT6-PENP39M53pVaP-d0PmngUuJ5c8alEX9ycBYaaDzL01pV5stPkfCPoy-ccTEgjGxcm5e2yUq131yL__oZDVvUc1s6mlGuyM6RWCzetpWjObUHBuDM93RZQmFJAH5MHmFUd5fjzmu7C8xgFcPbZbjoPY2lnXOYfa2ec7ktVdmn3713Iel4z9CwsHiBvaZ3H1VXm6c9IximyyD3zk0Tzi4f_aAlyKBHBIa8fR0_AE26QPsStIy3FMXvS8CUyO9Z2Wm0pN5qys6fZmpPjx5nPWhMBV13D3HdvBMJ_4vUVCdsUBYOrP5PvD=w640-h360-no)

You will need to download a PSP or mini Game/Demo from the official Playstation Store. Here is a very small list for
common regions:

EU/UK: Ape Quest  
NA/SG: LocoRoco Midnight Carnival  
JP: YS seven  

-----

![slide](https://lh3.googleusercontent.com/pYYk9b3Uju7r0rbb4y7NGdilN7hLc1J-KKhpfbSqBpahgvBap3M7LK5Inhr4GSzW6xkdzgWdyrQPjPMEtVV0kVA2H5HtExmZ9wFNkdUDx3s0EfLCYw4GywyU6veBakEITNOIylgEJaaK38L0WAZMV1apTLng59ZsTtrqU8hX8VtED8eqEyQV-YGcwhnagpdxYo0lQ_MRkxtQcD-TVGWrJkflETecc3yQZ96dZFiR_14I7RS6UQtaaOt_ISdxz101PTsjteM4Mq3bvZ8-H9Hx8LyHH1BTfisGqIH-Ycj3lMdbCvbOKxtHRmE_q0a3mvFF9wKYxn1o2XB4hbRMfKoMnEXR1uDH1i6VvvpVVXmsfi9JnwRulza3hVsWsxBAEV-fkEvDeHDFSx-M5oOKe9rHTKmwhCvjUtJjNjzDBQfcN8gr-64ETtqO4b8oT2bhm-n14OjsZemI1EipHobcl0ULIwp5ZMSLjR40rkx3Z-spbnzX_fUjOafma_pYqtW-KPMBfn9LrWVA2RWkBGQe_2Tn1Ko-60iqPQOj4Va6Xr7-Oyc9vBOR9FAd0BOFV-Zu3xbmJbhGzvsNPo1QDhwrGFPrQHRsc-5GcTXsNc7LpcKohg_V7Tlyzd8k9EdO30TCTw1owIfOKLKbWX3_swTyt5jGE4GSfnim7nLNqrjOXr5f3_8Ix392x3gjgXAJL27xMqQDAjum674jXUq-3nwGoOJ1T2L0=w640-h360-no)

Once you have your demo, confirm it will work by launching it and holding the Playstation button for a few seconds. If
you see a settings menu, it will work. Otherwise, try a different game because it does not appear this game is a PSP or
mini game.

-----

![slide](https://lh3.googleusercontent.com/E5z3BOlEMeDilMpzPbA0OluQFv11ngawWBdIHifmvjbz4Gr7COG1KfLJRZM_bBOcnwg2JmtSKUeZa_oqZi9c_MwQ1nzv6fv1zmuUHGCQUTqTzRTzR2IKw4xrBJPfBVL2Bge8_EF2g_Fn9-FzXsERttruyTsae4LInif-11QRV_ZSpk1Tp29LVRSrx8L8qtAPWzPpvdM0imSmplh05omvAmF_Cx9leZLe7tArLCwSsP48_1WVRnGa-nPF6SjjZKEudFaexaCjWXxG79RDzn1HysNTb4SL8dGHHBAyvXF4Mt7jhLY3kD4TIGaET0c6ubh8eBfl4ZgjnORew-3SoxvK2L9nfhiONc5xRbcNPDYc_Pk-yNfE9HNSzLuE1Iep7wTiZWE6QSUrAnPwF_Rg2S8b8ielPOdKdEaWTSztGAN2yICHEmmH4xe_DYskZFkjeBu0XrknZqLVx-MeKmgjWZm4fLHMowEnKHLhsWFtrzVoTwNfuuyYFeGUiJ0Y9zwwBigKstvQlT3tgCkMK-yrZRAhkNye9P0n9cfwIf3bQ0xRcIrI3kp8Gb0FWv85mdwd0RMI5Qh6I-LklSVou4bMLdFPDe-ksOEzMZKG2ToUGJT-J32QIKiejrKGYrWtQZZmbhhWzAxUzgY_APTe1NZWyyumtUW3sVbZpObFqaKgV1GGXSJZP5SMk4trAHDJsB5em3Jet6wIYuXg8U9P7E_0s2xVMu6T=w640-h360-no)

You will first begin by transferring the game to your PC. Ensure that QCMA is launched on your computer and your USB
cable is connected if you intend to connect via USB or you are on the same wireless network if you intend to connect via
Wi-Fi.

Launch **Content Manager** on your Vita and select **Copy Content**. Navigate to **System > PC**, **Applications**, 
**PSP/Other**. Select your game and copy it to your PC. 

-----

![slide](https://lh3.googleusercontent.com/GXK7GzFxKcPSDEthbr62qxF6ch9fDB3a5IjMQlv-QnoLhHbeoe8_XBsdnKvgweWHmVgcoMJEt7l-tcKasN-56zSyuq8-PX-kmvwK3o5BVUSOeddyEkv_DqnKtFcgDmDZszIFo3xw3wriOAHyGXynRFBCEEDmgwkFFAM7IIZbSPmtv_xW9LiLAWK_1UcEtD2sAf8MPu_nQz3p_pxSt9i0nXxdVr7GY7xkOnUVSslvfLewOp66UL48KIC4CusYeOJ8uLlFaf-13sfNelq6AfNFOizYclvyXMSWRHTmjYe62BvoDz46_cY25KQ_YXa4sT_Spn1jbMDP4uaxPHpR41Aqs5IQW8pY-UbmpwFtdir0MhiRJ634C_wprGOmW6TutUJ4xT4ikzpYaPrRdSuqRoyvL-TtOb5t8RQBAJN-nSzwIb1IUphCzOQTJ0XyxAUdd_9IQ3wNe7myslebMcl1-IDLloGWdvuMQdrUQjInd6KwXySwHt33tu4baBLY9bXGHQyzZaLuwRGSLR6tsvxJhxD8Wpfpre9XAYVFban5mH3XhkDTfCCteRVdrFxP1L0ScMAzLRsgdJGsbIMvOBQ1dIbK5FzsoR2BqG-McemfIgaKPJhzwrvFhBPGe9zk4OsP7BIvYTWJ-Pe86gJktkqFHxI_gEI41JU1_21UdD9mSsWvn7rax_wF1pV-wN71BY6RviV5fo67EkE1rOHFq6H-eLWBlcFR=w640-h360-no)

Once your game is on your PC, all you need to do is open the FinTrinity folder you downloaded and run one of two files
depending on which version you want to use.

**GUI.py**: Graphical User Interface (Slightly Prettier)  
**FinTrinity.py**: Command Line Interface (Gives a lot more information)

Follow the on-screen instructions to confirm the game you will be replacing with Trinity.

A folder will be placed on your Desktop that contains backups and files that were used. It's up to you, but I recommend
you keep at least the **[GAME_ID].backup** folder as that is the original game.

-----

![slide](https://lh3.googleusercontent.com/M8USa73J6PH5R_biRQZe_nhG5QWW78Q64c6GMbbIEPmUC7rk2YqnSe7h4YBL9QizAPIs_6Va1Re0flXhtijytTDCcMHGXhfPPMY2DCDa-D8E6AUZHy30ysmdfUN32xJG90jkyP7bQ9IImwPABgVGtc3_SG3UqwEI9T1Xq9vH1bi2qzWnM_uVvsIY2gPOk9RAFSv9eKcu4VdBBtz10Oc99rSoAKz7BwH90w4G5ikVpja9VkOENnNai_MGOSb8X2O752HN5qnUIKVHIJLP4QSf5IbqEe7oqf36nCCVEEgT_i7BkZ0CTSyDjDMf05EcuPbckYU4KYV_QqO0Pq5OiZgXH9FIg_-SraMskmY5ZKyPBcxXMhn7AEMapmg3obGXC8PEsX-EPB03mToGGvbB7KwMqTUeIKOC4M14BABd6RlFrEWw_nnZ-Bp2S7K0k-_df4uI2ru9qspWSHJ0l7E3nnnFTP7TD3Dl0Jr404pKKNd6EfKTuiLf-zwtmpr2NP8oQp3UG-UTFfHmqjZ6K_QCcEIKZYfbjUYhk8zV_4Hh88gM8__0kKrrwdklEgDHXOHxEtC-mmEHcl_vK7evKnnN233tJjAB32IsFN4ZxB8PbjhC-r0DwnrGHSPeTy-0ViSmKSrLmE3fQK1Zpql97j40L80AXyvIPjo6KMnk4k5MNr8VnsC1c-Cf0cQz5alYQSg4KsxoYxJd0je_HAjoCAcXBc-XYLbo=w640-h360-no)

Once FinTrinity has completed, you will need to right click on QCMA in your task bar and click **Refresh database**.

-----

![slide](https://lh3.googleusercontent.com/2AG_wbDIvL08LoEn_PsqWOoj1VCIlyhB6KzVL1x0XgusHm2vw0Zp0sRKbRseMhfCUbXb8YItXUkMDAp_riWqCFYvfCpIGUK6El6vzatwxbupkUH1zdrEKDqYrPlXpJq3bukhmPFeKj3j-8lyu9DdqSUlcx4kJc3JrLcRjw9mwOxlHuPn3XhN8xlEW4f1Heb0kGLf3IKneqz1nMhEM4Kr8LtTqegPQlJyLvQKgH-ZKMUO1Fm9SEe0azFjxUIXi2N1qkBt34xKC4OM-nW3253cvDncfQeSpVrlauAx8UGyIH7fL-gKvPQc8qEO6VaCIN15nvBSOY8_KxstH1_4UU7rYuNOCbnHudxE_x72WFWTrs3pncFHU9rZzbL6S2DJPimpP0rTE-QZSRmgCeSg1RHGXGk3S60Qx9oUaXBoNTlR8McEQi0ANlD1joxw549z1z8-CCabNpozYne4a8U1jvbipUZV8Cpj15FzOs5rWzJLESUuyMDSRAFjannBxBu5mIWfZzrkf7p4D_v4ckJpXKsPe6JxGuI3HCB_Va8ui0_fbI5jMlBNHsdX3kH8pDvaQuj7O8iiLQQRvtU_k-AgA6TabBWPl2IVIMeYe0VwXii7iWyk9wKsZREuZ7Mfn1DK4uceNNbtS2EtfC7jfasdYpTYhkviWohnIE7zGU5NtSN8pOZpbTtTYENYCn3EmPJfAwRLGusYU9uvgMGTn8gRXAWA7VNF=w640-h360-no)

Ensure that QCMA is launched on your computer and your USB cable is connected if you intend to connect via USB or you
are on the same wireless network if you intend to connect via Wi-Fi.

Launch **Content Manager** on your Vita and select **Copy Content**. Navigate to **PC > System**, **Applications**, 
**PSP/Other**. Select your game and copy it to your Vita. 

-----

![slide](https://lh3.googleusercontent.com/XTiZ8DAZEBgMV76gaWZJDSFSjh_xQj_VDubnNYWXU1VYZCQbms57bu-KllFswzYD_Nm3beA93KolmppDdJBeBdxZezY3upTYY36F8HKqzUdsEP3XaeHfK537HQn0VoRh6Mak1TVVc0toolpmN4dLeNJaoK2n2HdPeQ4wh2aDZmi-WXXZylAcRFUbkejn4jJYDcyo59p3-8o-a28RIkEFTcFDe-GHiYnqKfyoc3PXip5DWvjSfIqD0Dp9C4EYq3ujrSwqqK0e4Nmdqn2a6ZbMpCiXI-sCtXdC61jzb7Ru598diRyLo9UsZ4L3VRLJRiw6zuM7WkDhzuOjr3ce54iUqz9E0I49SfJ88c0RxyGI-iPlop41ibO5BDEBa8K8NuKmAcTia6RaKQWuvdNbUjNjol2rGdCM3fPT3NWrHrfUx-gsPFsGFvmBjNJHdTiNczoebOhGdRwJxGe5LaX8mqDZGnAaVfcAFxlpa9SNPWfPbZuJxgE__lqP1zfhedH-Z-uOaoWwC6qVC-Jx9rLz036gUbS5aIDEHOjBYVXX0V-33jxGvP7-VpE06G6Kvrdjco3FemutTZ2zoWp3UYpZpvSsSFtwEcxibaI2rNJ9dSN-7Z3lTOK0yiAgao5V_Q0-5S-kImYX2Ei-T4hxthBw73K7jUoDRIXEDkKyI26kYjSazvfi7ZHYhdhilmIBpxxBqQ7mZuDM-lTH9095kAO3oZeUv_al=w640-h360-no)

It is very important that Trinity is the very first thing you run after a boot, so reboot your Vita.

Additionally, you need to make sure that there are no active downloads taking place. If there are, either wait until
they finish and reboot again, or cancel them.

-----

![slide](https://lh3.googleusercontent.com/qsX3EvKfPStqvOJx2ZQyGROPmJEG6bsEhfXPVll021cV_7ULXrZTPWzKiSQVEO6UWdKdNeXpx83pItTz7OBngzORT7Qmb9qlxBdYyog3D4sZaAvaE2qYsFjjooa6pnRN-akflaGDkdPVM01r4Ekn-K-2wL2YvhY8jxtLeyvuGiL_Hbrceu7R-TICAaiFIYGmJQ8t9HxL6Ty7ujnyva-KmfvjWXS-kGMUhm95OJkLMDx5gTawqu_4pHsuLENfbeMiRzJDIgGFOnRoFTIsrH9kMOKIhRTXtEDcqViWx_IjqCEhBZ2rEvhl2Ivhu2MwPzTQsK0cCWrjCbYxogwJkqqjcR1mM6gEHi1HDS7jsKrtDLkHeANLCDFEVSNAHUiAaXI7Flygx-oh_h9BmLgvEVCcA2jSy1rY0L543OV0OJbldZ7hIzyA2VrjhBubJD7k0NdW3Sq5OkY_TemuYzjOPZpb45hBBp-FCY8Z1CoGmKPHnbxu8i0wK8afpzb3YzP7jlIr4uWUFYSp-6gGQPsLzFFHTYfC4m14aB5HxSaWsH1H-XO8ofTGjH6nm3Xy_gluU60eG3vGmWV0w8LfmJw2mYVKr8ElaUSA8TNjOxEdxz7ZOd3ZGHiMtEDL-prOfYi34MKSdH8usNmqK93mKYCsXoMrx0WVM96rYyq88S4CMW_jrKLnXJEyxiz9qN2GCvwG9cx4kEr8LoqrQ99dFtYyodveQLtm=w640-h360-no)

You should now see a Trinity icon on your Vita. Run this and you should see a sweet splash screen, then the
**Construct** menu should launch.

Select **Install HENkaku** and insure it installs.

Select **Download VitaShell** and insure it installs.

**Note**  
If you receive an error while trying to install VitaShell, make sure your WiFi is turned on.

-----

![slide](https://lh3.googleusercontent.com/v5leKMTKvDTBiL87x7fs2cYw2HtO1OO0zbLEXwDgF0sAGsRWPHhsEJt8OvjoVjrLmdPRK7Xw4YztvqV745-eC4naCfwK3X2HWFANttF7L0yGuoHD-BLBROEirrm6f29HuVE4dEI4XzCZ_dRh6fSxQ__uqPvaemqoDDSGWE96gSl168kzhy_vxa8DweOxzkuNU07gaAkoQyVH-On7DAmpE2xvaY6NOtu9ZCEIL5I_AC_9lsdnaIgYPapxR0s6cOQzvoG1T3qKOIGmzaSR0zoJT2l4sbLSgJlyXi81FvxEMT5OaJKTMbQsd70cOddaBss-BUJq0274xejUMv8Q08dUUauasHKG9RRv5huVyWq7vsXlBd0AIs3t8qhkHk8rFghPDFFt9tNsZiCs6cJOgKRitWIDRHjfxPEDS5e58vZFsHocRkaU84X2-LK-P1Ko1avoUTUpsnZQav9sXBKOvhyU0iYALa907KWyYdSLcXk-_vs6YpjtPszOjIJl-10aKyr53-6ylzsdwH-KoDf5AcPxhKdKQtSjQW3fXSitLZURkmiaevgRU_6gaQaJ_xdu2hnCEaAA5KG_iNxeFi76LsVLPwuzCzqvKqO4evLLZo4qk0aY4HWdyhrRroEqfhHDyFztlFPmPsL4OQrJSuVR7638SouHJxbONS3sFKRM67qOMI1KQv6hQLwjPQXVGqyqyLl-lU-Z9BX5eVU-goxs0J1M0zk9=w640-h360-no)

You should now have HENkaku installed on your 3.70 Firmware.

The VERY first thing that you should do is downgrade to a lower firmware. I personally like Firmware 3.65, but many 
others recommend Firmware 3.60. Either of them will be better. After you downgrade, you will have to hack your Vita
again for that firmware, but this will ensure that you are able to have a hacked Vita forever. It is currently unknown
how Sony will react to Trinity, but it is very possible that they will make an attempt to stop it. Older methods are
more resistant to Sony's attempts.

You can check out my other videos for help with this:  
* [Downgrade with Modoru](https://www.bytepen.com/hack-the-playstation-vita?v=downgrade-with-modoru)
* [Install HENkaku on 3.60](https://www.bytepen.com/hack-the-playstation-vita?v=install-henkaku-on-firmware-3-60)
* [Install h-encore on 3.65-3.68](https://www.bytepen.com/hack-the-playstation-vita?v=install-h-encore-on-365-368)
* [Install Enso](https://www.bytepen.com/hack-the-playstation-vita?v=install-enso-on-firmware-365)


----------
VideoID:bGyBL5EASko