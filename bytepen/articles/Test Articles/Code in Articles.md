
```js
function sayHello (msg, who) {
    return '${who} says: ${msg}';
}
sayHello("Hello World", "Johnny");
```

```bash
#!/bin/bash

main() {
    function1
    function2
    function3 ${@}
}

function1() {
    echo "Hello"
}

function2() {
    echo "There"
}

function3() {
    echo ${@}
}

main ${@}

```

```shell
echo "Hello, World!"
```

```root-shell
cat /etc/shadow
```

```generic
This is just some generic text
```


This sentence has a bit of `inline code` within it!

----------
VideoID:CodeInArticles