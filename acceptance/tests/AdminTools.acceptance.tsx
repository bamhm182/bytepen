/// <reference path="../steps.d.ts" />

Feature('Admin Tools');

Scenario('should provide a working video length checker', (I) => {
    I.amOnPage('/vlc');

    I.waitForElement('.introSection', 10);
    I.fillField('.introSection > textarea', 'This is a test');
    I.see('0.7 Seconds', '.introSection > .time');

    I.waitForElement('.setupSection', 10);
    I.fillField('.setupSection > textarea', 'This is a test');
    I.see('0.7 Seconds', '.setupSection > .time');

    I.waitForElement('.contentSection', 10);
    I.fillField('.contentSection > textarea', 'This is a test');
    I.see('0.7 Seconds', '.contentSection > .time');
});