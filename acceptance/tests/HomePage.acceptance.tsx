/// <reference path="../steps.d.ts" />

Feature('Home Page');

Scenario('should render a banner with a menu', (I) => {
    I.amOnPage('/');
    I.seeElement('.img');
    I.see('BytePen Studios', '.title');
    I.see('Home', '.menu');
    I.see('Tutorials', '.menu');
    I.see('Projects', '.menu');
    I.see('Support Me', '.menu');
    I.moveCursorTo('.Tutorials');
    I.see('My Important Playlist');
});

Scenario('should render a home page with the most recent article and video', (I) => {
    I.amOnPage('/');
    I.see('Welcome to BytePen Studios!');
    I.seeElement('.video');
    I.seeElement('.article');
});