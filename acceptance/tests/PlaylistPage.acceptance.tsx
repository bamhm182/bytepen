/// <reference path="../steps.d.ts" />

Feature('Playlist Page');

Scenario('should render a list of videos', (I) => {
    I.amOnPage('/my-test-playlist');
    I.waitForElement('.video', 10);
    I.waitForElement('.article', 10);
});

Scenario('should render a video based on the get request', (I) => {
    I.amOnPage('/my-test-playlist?v=video-2');
    I.waitForElement('.video', 10);
    I.waitForElement('.article', 10);
    I.see('This is the article for video 2 of my test playlist.');
});
