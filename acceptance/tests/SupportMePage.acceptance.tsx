/// <reference path="../steps.d.ts" />
const assert = require('assert');

Feature('Support Me Page');

Scenario('should render a list of ways to support me', async (I) => {
    I.amOnPage('/support-me');
    I.waitForElement('.modules', 10);
    const moduleCount = await I.grabNumberOfVisibleElements('.module');
    assert.strictEqual(moduleCount, 7);
});